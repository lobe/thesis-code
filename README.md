Here is how to use this repo. There are lots of discrete moving parts that combine to work well together

1. Generate attack files

``attack_code/generate.py -b attacks/in/u1.base -t attacks/in/u1.test -s Random None Average Bandwagon -o attacks/out/ -f``

This creates attack files.
-b is for base file
-t is for test file
-s is for what attack strategies to employ
-o is for where to place the output files
-f is for force, or overwrite

2. Make changes to a recommender, or add a new one

in solutions.txt, the subdirectories each contain a different recommender (except common). It consists of a CMake file and sources.

2.a. Modify an existing recommender
2.b. Add a new one. Follow the existing pattern to use the files from common. Modify the cmake file with the recommender name (same as dir). Be sure to add file to the solutions/solutions.txt file, one per line
2.c. Optional -> solutions/makeall.sh will make all active recommenders. Only needed when testing locally

3. Run automated tests. This will run all active algorithms against all generated attacks files

``pyscripts/runRecommendations.py``

A lot is going on behind the scenes here. Fix up the config in this file before you run it to know where the tests will be run. supports remote hosts.

Here's what happens:

3.a. Attacks and solutions are copied to remote host (may be local). Changes must have been committed to git (so there is source code / tests to match against the results)
3.b. Recommenders are made into binaries via the makeall.sh script. Only active ones.
3.c. All recommenders are run against all attacked files (and unattacked files). Results are placed in same directory as attack files, and info about test is contained in .meta files
3.c.1. Note that only ratings present in the test file are produced via the executor.sh script. This is to save on disk usage (20M -> 1M)
3.d. Analysis program is run on each directory with ratings. Statistics about the run are generated from recommendations, test files, and meta files.
3.e. Analysis combination program is run. This takes all the previously produced analysis in all sub directories, and makes a single html file with a summary of all results
3.f. Analysis summary uploaded to S3. Entire attack directory (now including recommendations and analysis) also uploaded to S3.

Current a-c are implemented. d-e are currently in progress. Still deciding whether to do f by hand or use a build server to trigger these builds.


