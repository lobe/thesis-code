#include <vector>
#include <map>
#include <algorithm>
#include <math.h>
#include <iostream>

using namespace std;

// Just to play and test with Aleks algo
// Set params
double alpha = 2.0;
double epsilon = 0.00000001;	

double nNorm(const vector<double>& input, int n);

int main(int argc, char*argv[])
{

	// voter -> item -> rating
	map<unsigned int, map<unsigned int, unsigned int>> ratings;
	ratings[1] = {{1, 1}, {2, 1}, {3, 3}, {4, 1}, {5, 2}, {6, 1}};
	ratings[2] = {{1, 1}, {2, 2}, {3, 4}, {4, 3}, {5, 2}, {6, 2}};
	ratings[3] = {{1, 1}, {2, 2}, {3, 4}, {4, 3}, {5, 2}, {6, 2}};
	ratings[4] = {{1, 2}, {2, 3}, {3, 4}, {4, 3}, {5, 1}, {6, 1}};
	ratings[5] = {{1, 2}, {2, 2}, {3, 2}, {4, 1}, {5, 1}, {6, 1}};

	// Transform ratings to be item / rating based
	// Item -> Rating Given -> Vector of users giving that rating
	map<unsigned int, map<unsigned int, vector<unsigned int>>> givers;
	for (auto user = ratings.begin(); user != ratings.end(); ++user)
	{
		for (auto item = user->second.begin(); item != user->second.end(); ++item)
		{
			auto itemGiver = givers.find(item->first);
			if (itemGiver == givers.end()) {
				itemGiver = givers.insert(make_pair(item->first, map<unsigned int, vector<unsigned int>>())).first;
			}
			// Now find the entry for the rating
			auto ratingGiver = itemGiver->second.find(item->second);
			if (ratingGiver == itemGiver->second.end()) {
				ratingGiver = itemGiver->second.insert(make_pair(item->second, vector<unsigned int>())).first;
			}
			// Now insert the user to the rating entry
			ratingGiver->second.push_back(user->first);
		}
	}


	map<unsigned int, double> trust;
	for (auto votes = ratings.begin(); votes != ratings.end(); ++votes)
	{
		trust[votes->first] = 1.0;
	}
	// Calculate credibility
	// item -> rating given -> credibility
	map<unsigned int, map<unsigned int, double> > credibility;
	// Zero fill credibility (will be properly set in loop)
	for (auto itemIt = givers.begin(); itemIt != givers.end(); ++itemIt)
	{
		auto r = itemIt->second;
		credibility.insert(make_pair(itemIt->first, map<unsigned int, double>()));
		for (auto ratingIt = r.begin(); ratingIt != r.end(); ++ratingIt)
		{
			credibility.at(itemIt->first).insert(make_pair(ratingIt->first, 0));
		}
	}

	// Keep a vector of all delta's to computer termination
	vector<double> deltas;

	while (true)
	{
		deltas.clear();

		// Compute credibility

		// Now calculate credibility
		for (auto itemIt = credibility.begin(); itemIt != credibility.end(); ++itemIt)
		{
			double itemSum = 0.0;
			vector<double> itemDeltas;
			for (auto ratingIt = itemIt->second.begin(); ratingIt != itemIt->second.end(); ++ratingIt)
			{
				itemDeltas.push_back(ratingIt->second); // Save the trust level for the given rating
				double ratingSum = 0.0;
				ratingIt->second = 0.0;
				// Loop through all users that gave that item a given rating
				// and add their Trust to the score;
				auto users = givers.at(itemIt->first).at(ratingIt->first);
				for (auto userIt = users.begin(); userIt != users.end(); ++userIt)
				{
					ratingIt->second += pow(trust[*userIt], alpha);
					ratingSum += pow(trust[*userIt], alpha);
				}
				itemSum += pow(ratingSum, 2);
			}
			// Now divide every rating level by the root of the item sum. Compare with itemDelta and add to deltas
			itemSum = sqrt(itemSum);
			auto deltaIt = itemDeltas.begin();
			for (auto ratingIt = itemIt->second.begin(); ratingIt != itemIt->second.end(); ++ratingIt)
			{
				ratingIt->second /= itemSum;
				deltas.push_back(*deltaIt - ratingIt->second);
				++deltaIt;
			}
		}

		// Break if at convergence
		double difference = nNorm(deltas, 2);
		cout << "Difference is " << difference << endl;
		if (difference < epsilon)
			break;

		// Compute trust ratings
		// First reset all trust to 0
		for (auto it = trust.begin(); it != trust.end(); ++it)
		{
			it->second = 0.0;
		}

		cout << "Calculating trust" << endl;

		// Secondly, calculate trust scores agains
		for (auto user = ratings.begin(); user != ratings.end(); ++user)
		{
			for (auto item = user->second.begin(); item != user->second.end(); ++item)
			{
				trust[user->first] += credibility.at(item->first).at(item->second);
			}
		}

		cout << "trust calculated" << endl;
	}

	return 0;
}

double nNorm(const vector<double>& input, int n)
{
	double norm;
	for (auto val: input)
	{
		norm += pow(val, n);
	}
	return pow(norm, (double)1/n);
}