FILE FORMATS
============

Attack files contain the following metadata:
  * Normal Metadata: i.e. details of file in total
  * Attack Algorithm: The method of attack used (Random, Average, Bandwagon)
  * Filler Size: Average Number of filler items used
  * Attack Size: Number of attack profiles used in each attack
  * Attacked Items: The items that were attacked. Also serves as # of attacks. 1 attack for 1 item
  * Attack Users: The injected profiles used for the attack, and the corresponding items they attacked
  
This will be followed by the following data format:
1 2 5 (User 1 rated item 2 5 stars)
2 2 4
etc.


Combined, an example file may look like:

Metadata:
    File:
        Items: 1500
        Users: 1800
        Legitimate Users: 1500
        Attack Users: 300
    Attack:
        Algorithm: Average
        Filler Size: 50
        
Ratings: