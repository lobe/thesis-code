import abc
from abc import ABCMeta
from ratings import Rating
import json
from enum import Enum
import statistics
import random
import operator


def validate_rating(rating: float) -> int:
    return max(min(int(rating), 5), 1)

def sample_ratings(ratings, percentage):
    return random.sample(ratings, int(len(ratings) * percentage / 100))


class AttackType(Enum):
    none = 0,
    nuke = 1,
    push = 2,


class Attack(metaclass=ABCMeta):

    def metadata(self):

        meta = {
            "shill users": self.attack_users,
            "attacked items": self.attacked_items,
            "attack type": self.attack_type.name,
            "avg filler size": self.filler_size,
            "attack strategy": self.algorithm
        }

        return json.dumps(meta)


class NoAttack(Attack):
    algorithm = "None"
    attack_type = AttackType.none
    filler_size = 0.0
    ratings = []
    attack_ratings = []
    attacked_items = []
    attack_users = []



class RandomAttack(Attack):
    """
    filler size: percentage filler size
    attack size:

    """

    algorithm = "Random"

    def __init__(self, base_ratings: list, filler: float, attack_size: float, to_attack: list, attack_type: AttackType):
        self.attack_type = attack_type
        self.ratings = base_ratings
        self.filler_size = filler
        self.attack_size = int(len(set(map(lambda u: u.user, self.ratings))) * attack_size / 100)
        self.attacked_items = to_attack

        mean_vote = statistics.mean(map(lambda r: r.rating, self.ratings))
        max_user = max(map(lambda r: r.user, self.ratings))

        self.attack_users = list(range(max_user + 1, max_user + self.attack_size + 1))  # List of attack users
        self.attack_ratings = []

        print ("Generating for attack: " + self.algorithm)
        for attack_user in self.attack_users:
            print("Processing user " + str(attack_user - max_user) + " of " + str(max(self.attack_users) - max_user))
            for a in to_attack:
                if attack_type == AttackType.push:
                    bad_rating = Rating(attack_user, a, validate_rating(random.normalvariate(5.0, 0.5)))
                else:
                    bad_rating = Rating(attack_user, a, validate_rating(random.normalvariate(1.0, 0.5)))
                self.attack_ratings.append(bad_rating)
            # Now add the filler
            for item in sample_ratings(set(map(lambda x: x.item, filter(lambda v: v.item not in to_attack, self.ratings))), self.filler_size):
                self.attack_ratings.append(Rating(attack_user, item, validate_rating(random.normalvariate(mean_vote, 1.0))))


class AverageAttack(Attack):
    """

    """
    algorithm = "Average"

    def __init__(self, base_ratings: list, filler: float, attack_size: float, to_attack: list, attack_type: AttackType):
        self.attack_type = attack_type
        self.ratings = base_ratings
        self.filler_size = filler
        self.attack_size = int(len(set(map(lambda u: u.user, self.ratings))) * attack_size / 100)
        self.attacked_items = to_attack

        ratings_by_item = {i: [] for i in set([r.item for r in self.ratings])}
        for r in self.ratings:
            ratings_by_item[r.item].append(r.rating)
        mean_rating_by_item = {k: statistics.mean(v) for k, v in ratings_by_item.items()}

        max_user = max(map(lambda r: r.user, self.ratings))

        self.attack_users = list(range(max_user + 1, max_user + self.attack_size + 1))  # List of attack users
        self.attack_ratings = []

        print ("Generating for attack: " + self.algorithm)
        for attack_user in self.attack_users:
            print("    Processing user " + str(attack_user - max_user) + " of " + str(max(self.attack_users) - max_user))
            for a in to_attack:
                if attack_type == AttackType.push:
                    bad_rating = Rating(attack_user, a, validate_rating(random.normalvariate(5.0, 0.5)))
                else:
                    bad_rating = Rating(attack_user, a, validate_rating(random.normalvariate(1.0, 0.5)))
                self.attack_ratings.append(bad_rating)
            # Now add the filler
            for item in sample_ratings(set(map(lambda x: x.item, filter(lambda v: v.item not in to_attack, self.ratings))), self.filler_size):
                self.attack_ratings.append(Rating(attack_user, item, validate_rating(random.normalvariate(mean_rating_by_item[item], 1.0))))


class BandwagonAttack(Attack):
    """
    filler size: percentage filler size
    attack size:

    """

    algorithm = "Bandwagon"
    bandwagon_percentage = 0.10  # Give top 10% of items max ratings

    def __init__(self, base_ratings: list, filler: float, attack_size: float, to_attack: list, attack_type: AttackType):
        self.attack_type = attack_type
        self.ratings = base_ratings
        self.filler_size = filler
        self.attack_size = int(len(set(map(lambda u: u.user, self.ratings))) * attack_size / 100)
        self.attacked_items = to_attack

        mean_vote = statistics.mean(map(lambda r: r.rating, self.ratings))
        max_user = max(map(lambda r: r.user, self.ratings))

        ratings_by_item = {i: [] for i in set([r.item for r in self.ratings])}
        for r in self.ratings:
            ratings_by_item[r.item].append(r.rating)
        mean_rating_by_item = {k: statistics.mean(v) for k, v in ratings_by_item.items()}
        items_sorted_by_rating = [k for k, v in sorted(mean_rating_by_item.items(), key=operator.itemgetter(1), reverse=True)]
        bandwagon = items_sorted_by_rating[0: int(len(items_sorted_by_rating) * self.bandwagon_percentage)]

        self.attack_users = list(range(max_user + 1, max_user + self.attack_size + 1))  # List of attack users
        self.attack_ratings = []

        print ("Generating for attack: " + self.algorithm)
        for attack_user in self.attack_users:
            print("Processing user " + str(attack_user - max_user) + " of " + str(max(self.attack_users) - max_user))
            for a in to_attack:
                if attack_type == AttackType.push:
                    bad_rating = Rating(attack_user, a, validate_rating(random.normalvariate(5.0, 0.5)))
                else:
                    bad_rating = Rating(attack_user, a, validate_rating(random.normalvariate(1.0, 0.5)))
                self.attack_ratings.append(bad_rating)
            # Now add the filler
            for item in sample_ratings(set(map(lambda x: x.item, filter(lambda v: v.item not in to_attack, self.ratings))), self.filler_size):
                if item in bandwagon:
                    self.attack_ratings.append(Rating(attack_user, item, validate_rating(5.0)))
                else:
                    self.attack_ratings.append(Rating(attack_user, item, validate_rating(random.normalvariate(mean_vote, 1.0))))