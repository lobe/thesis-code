import argparse
import os
import sys
from ratings import process_ratings
import attack
import shutil
import random
from operator import attrgetter

Attacks = ["None", "Bandwagon", "Average", "Random"]
allowedAttackType = {
    "None": [attack.AttackType.none],
    "Random": [attack.AttackType.push, attack.AttackType.nuke],
    "Average": [attack.AttackType.push, attack.AttackType.nuke],
    "Bandwagon": [attack.AttackType.push, attack.AttackType.nuke],
}
config = {
    "baseFile": "attacked.base",
    "testFile": "ratings.test",
    "metaDataFile": "attack.meta",
    "originalBase": "unaltered.base"
}

class ValidationError(Exception):
    def __init__(self, message):
        self.message = message

def write_ratings(dest, ratings):
    with open(dest, "w") as f:
        for r in ratings:
            f.write("{user} {item} {rating}\n".format(user=r.user, item=r.item, rating=r.rating))


def destinationDir(outDir, inFile, strategy):
    return outDir + "/" + inFile.split("/")[-1].split(".")[0] + "/" + strategy + "/"

def currAttemptDir(outDir, inFile, strategy, currAttempt):
    return destinationDir(outDir, inFile, strategy) + str(currAttempt) + "/"

def ensureDirExists(dir):
    if not os.path.isdir(dir):
        os.makedirs(dir)
    return dir

def validArgs(inFile, testFile, outDir, strategies, forceWrite):
    if not os.path.exists(outDir):
        os.makedirs(outDir)

    if not os.path.isdir(outDir):
        raise ValidationError("Output Directory is not valid")

    if not os.path.isfile(inFile):
        raise ValidationError("Input File does not exist")

    if not os.path.isfile(testFile):
        raise ValidationError("Test File does not exist")

    for strategy in strategies:
        if strategy not in Attacks:
            raise ValidationError("Invalid Attack Strategy, possible values are: " + " ".join(Attacks))

    for strategy in strategies:
        destDir = destinationDir(outDir, inFile, strategy)
        if os.path.exists(destDir) and not forceWrite:
            raise ValidationError("Attempting to overwrite Strategy File")
        elif not os.path.exists(destDir):
            os.makedirs(destDir)

    return True




def main():
    parser = argparse.ArgumentParser(description="Attack generation script")

    parser.add_argument('--basefile', '-b', required=True)
    parser.add_argument('--testfile', '-t', required=True)
    parser.add_argument('--strategy', '-s', default='None', nargs='+')
    parser.add_argument('--outputDir', '-o', default='out')
    parser.add_argument('--force', '-f', default=False, action="store_true")

    parsed = parser.parse_args()
    baseFile = parsed.basefile
    testFile = parsed.testfile
    outDir = parsed.outputDir.rstrip("/")
    strategies = parsed.strategy
    forceWrite = parsed.force

    try:
        validArgs(baseFile, testFile, outDir, strategies, forceWrite)
    except ValidationError as e:
        print(e.message)
        sys.exit(1)

    with open(baseFile, "r") as f:
        ratings = process_ratings(f.readlines())

    for strategy in strategies:
        toAttack = random.choice([r.item for r in ratings])
        for attack_type in allowedAttackType[strategy]:
            if strategy == "None":
                attack_strategy = attack.NoAttack()
            elif strategy == "Random":
                attack_strategy = attack.RandomAttack(ratings, 5.0, 3.0, [toAttack], attack_type)
            elif strategy == "Average":
                attack_strategy = attack.AverageAttack(ratings, 5.0, 3.0, [toAttack], attack_type)
            elif strategy == "Bandwagon":
                attack_strategy = attack.BandwagonAttack(ratings, 5.0, 3.0, [toAttack], attack_type)
            else:
                raise RuntimeError("Invalid strategy")

            attack_ratings = sorted(attack_strategy.attack_ratings, key=attrgetter('user', 'item'))
            combined_ratings = ratings + attack_ratings
            target_directory = ensureDirExists(currAttemptDir(outDir, baseFile, strategy, attack_type.name))
            write_ratings(target_directory + config["baseFile"], combined_ratings)
            write_ratings(target_directory + config["originalBase"], ratings)
            shutil.copyfile(testFile, target_directory + config["testFile"])
            with open(target_directory + config["metaDataFile"], "w") as mdf:
                mdf.write(attack_strategy.metadata())

            if strategy == "None":
                break  # No need to try push and nuke for no attack


if __name__ == "__main__":
    main()

