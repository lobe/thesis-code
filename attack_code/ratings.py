from collections import namedtuple

Rating = namedtuple('Rating', 'user item rating')

# Take in a list of inputs in format "user item rating" and get a list of those ratings
def process_rating(line):
    user, item, rating = map(int, line.split()[0:3])
    return Rating(user, item, rating)


def process_ratings(input):
    return list(map(process_rating, input))

def formatRating(rating: Rating) -> str:
    return "% % %".format(rating.user, rating.user, rating.rating)