#!/bin/bash

cd `dirname "$0"`

find attacks -type f -name "*.analysis" -delete
find attacks -type f -name "analysis.aggregate" -delete
find attacks -type f -name "unaltered.*" -a ! -name "unaltered.base" -delete
find attacks -type f -name "attacked.*" -a ! -name "attacked.base" -delete
find solutions -type d -name "bin" -exec rm -rf "{}" \; 2> /dev/null
rm executor.log
