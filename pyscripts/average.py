#!/usr/bin/python

import fileinput
import sys

ratings = []

for line in fileinput.input():
    ratings.append(float(line.split()[2]))

def average(r):
    return float(sum(r)) / float(len(r))

av = average(ratings)

print("Average: " + str(av))
print("Internal Average " + str(av / 4 - 0.25))
