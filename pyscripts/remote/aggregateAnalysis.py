#!/usr/bin/python3

import os
import sys
import json

# Find analysis files lying beneath current target directory (1st arg), read their json and output in a single file in
# target directory

out_file = "analysis.aggregate"
summary_file = "analysis.summary"

try:
    target_dir = sys.argv[1]
except KeyError:
    print("First arg must be set as target dir")
    sys.exit(1)

aggregate = {}
for dpath, dnames, fnames in os.walk(target_dir):
    for fname in fnames:
        if fname.endswith(".analysis"):
            method = fname.split(".")[0]

            if dpath not in aggregate:
                aggregate[dpath] = {}
            if 'quickSummary' not in aggregate[dpath]:
                aggregate[dpath]['quickSummary'] = {}
            if 'results' not in aggregate[dpath]:
                aggregate[dpath]['results'] = {}

            with open(os.path.join(dpath, fname)) as handle:
                analysis = json.load(handle)
                if 'attackData' not in aggregate[dpath]:
                    aggregate[dpath]['attackData'] = analysis['attackData']
                aggregate[dpath]['quickSummary'][method] = analysis['quickSummary']
                aggregate[dpath]['results'][method] = analysis['results']

with open(os.path.join(target_dir, out_file), "w") as f:
    f.write(json.dumps(aggregate, indent=4, sort_keys=True))

with open(os.path.join(target_dir, summary_file), "w") as f:
    summary_contents = {}
    for dpath in aggregate:
        summary_contents[dpath] = {}
        summary_contents[dpath]['attackData'] = aggregate[dpath]['attackData']
        summary_contents[dpath]['quickSummary'] = aggregate[dpath]['quickSummary']
    f.write(json.dumps(summary_contents, indent=4, sort_keys=True))