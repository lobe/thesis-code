#!/usr/bin/python3

import json
import math
import pprint
import glob
from operator import itemgetter
import copy

# Perform Analysis on output files

# 1. Read attack.meta
#   Get list of all attacked items
# 2. Read in ratings.test. Store all ratings in memory
# 3. Read unaltered.*
#   read all ratings into memory (may be better to a lazy side by side, will look at later)
# 4. Read ratings.test into memory
# 5. Read in attacked.* (and compute statistics)
#   - Using respective unaltered file
#       - For each attacked item, compute the mean and median rating for both base and the unaltered and
#       - For each attacked item, for each user compute the ranking that rating gives them
#   - Using ratings.test ratings
#       - Compute SMAE (or whatever it is) for all ratings that also appear in ratings.test
# 6. Jsonify results, and add to output file along with necessary attack info from attack.meta

topk = [5, 10, 50, 100, 500]

test_file = "ratings.test"
metadata_file = "attack.meta"
output_suffix = "analysis"

unaltered_prefix = "unaltered"
attacked_prefix = "attacked"
base_suffix = "base"

# Shitty way to stop reading files all the time. CBF doing something cleaner
rating_cache = {}
shill_users = []

# ## FOR DEV ONLY, POINT TO A DIR TO DO ANALYSIS IN
# os.chdir("/home/hamish/thesis/trials/539778d879f1d93b9a44c3bbb0942c3119223bb2/attack/out/u1/Average/nuke")

def scanFiles():
    methods = set()
    attack_statuses = set()
    for file in set(glob.glob(unaltered_prefix + ".*") + glob.glob(attacked_prefix + ".*")) - set(glob.glob("*." + base_suffix)):
        attack_status, method = file.split(".")[0:2]
        methods.add(method)
        attack_statuses.add(attack_status)
    return methods, attack_statuses


# Read in ratings, return a dictionary of type (user,item): rating
def get_ratings(method, attack_status):
    fileName = attack_status + "." + method
    try:
        return rating_cache[method][attack_status]
    except KeyError:
        pass
    ratings = read_ratings(fileName)
    if method not in rating_cache:
        rating_cache[method] = {}
    rating_cache[method][attack_status] = ratings
    return ratings

def get_test_ratings():
    test_ratings = {}
    for f in [test_file, unaltered_prefix + "." + base_suffix, attacked_prefix + "." + base_suffix]:
        try:
            test_ratings[f] = rating_cache[f]
        except KeyError:
            rating_cache[f] = read_ratings(f)
            test_ratings[f] = rating_cache[f]

    return test_ratings

def read_ratings(fileName):
    print("READING RATINGS of " + fileName)
    ratings = {}
    with open(fileName) as f:
        for line in f:
            if not line.strip():
                continue
            user, movie, rating = line.split()[0:3]
            user = int(user)
            movie = int(movie)
            rating = float(rating)
            # Don't want to analyse data from the fake users
            if user in shill_users:
                continue
            if user not in ratings:
                ratings[user] = []
            ratings[user].append((movie, rating))
    return ratings

# Convert from user -> [(item, rating)] to (user, item) -> rating
def dictRatings(oldRatings):
    newRatings = {}
    for user, list in oldRatings.items():
        for compound in list:
            movie, rating = compound
            newRatings[(user, movie)] = rating
    return newRatings

def root_mean_square_error(baseRatings, comparisonRatings):
    new_base = dictRatings(baseRatings)
    new_comparison = dictRatings(comparisonRatings)
    differences = []
    for key, rating in new_base.items():
        try:
            secondRating = new_comparison[key]
            differences.append(rating - secondRating)
        except KeyError:
            pass
    return math.sqrt(sum(map(lambda x: x*x, differences)) / len(differences))

def mean_abs_error(baseRatings, comparisonRatings):
      new_base = dictRatings(baseRatings)
      new_comparison = dictRatings(comparisonRatings)
      differences = []
      for key, rating in new_base.items():
          try:
              secondRating = new_comparison[key]
              differences.append(rating - secondRating)
          except KeyError:
              pass
      return sum(map(abs, differences)) / len(differences)

def get_all_rmse(test_ratings, methods, attack_statuses):
    rmse = {}
    for method in methods:
        for attack_status in attack_statuses:
            file_ratings = get_ratings(method, attack_status)
            if method not in rmse:
                rmse[method] = {}
            rmse[method][attack_status] = {
                "training": root_mean_square_error(test_ratings[attack_status + "." + base_suffix], file_ratings),
                "testing": root_mean_square_error(test_ratings[test_file], file_ratings),
                "mean abs error training": mean_abs_error(test_ratings[attack_status + "." + base_suffix], file_ratings),
                "mean abs error testing": mean_abs_error(test_ratings[test_file], file_ratings)
            }
            # rmse[method][attack_status] = root_mean_square_error(test_ratings, file_ratings)
    return rmse

def getPredictionRanks(inputRatings, items):
    # Sort by rating descending, remove ratings. Don't want to augment outside this function
    print("    Starting Deep Copy")
    ratings = copy.deepcopy(inputRatings)
    print("    Starting Sort")
    for user in ratings:
        ratings[user].sort(key=itemgetter(1), reverse=True)
        ratings[user] = [compound[0] for compound in ratings[user]] # Remove rating, keep order

    print("    Starting Rank calculations")
    results = {}
    for item in items:
        results[item] = {}
        for user in ratings:
            results[item][user] = (ratings[user].index(item) + 1, len(ratings[user]))
    return results

def getAllPredictionRanks(methods, attack_statuses, items):
    results = {}
    for method in methods:
        results[method] = {}
        for attack_status in attack_statuses:
            print("Starting predRank for " + method + ":" + attack_status)
            results[method][attack_status] = getPredictionRanks(get_ratings(method, attack_status), items)
    return results

def predShift(predRanks):
    results = {}
    for item in predRanks[unaltered_prefix]:
        results[item] = {}
        for user in predRanks[unaltered_prefix][item]:
            results[item][user] = {}
            rank, total = predRanks[unaltered_prefix][item][user]
            results[item][user] = (rank - predRanks[attacked_prefix][item][user][0], total)
    return results

def hitRatio(predRanks, k):
    results = {}
    for item in predRanks[unaltered_prefix]:
        results[item] = {}
        tally = 0
        for user in predRanks[unaltered_prefix][item]:
            baseRank, total = predRanks[unaltered_prefix][item][user]
            attackRank, _ = predRanks[attacked_prefix][item][user]
            if attackRank <= k and baseRank > k:
                tally += 1
            if attackRank > k and baseRank <= k:
                tally -= 1
        results[item] = tally * 100 / total
    return results

def avgShift(predShifts):
    results = {}
    for item in predShifts:
        results[item] = {}
        shiftSum = sum(x[0] for x in predShifts[item].values())
        total = list(predShifts[item].values())[0][1] # Take any total
        results[item]["avgShift"] = shiftSum / total
        results[item]["Max"] = total
    return results


def main():
    global shill_users
    with open(metadata_file) as mfile:
        attackData = json.load(mfile)
        shill_users = attackData["shill users"]

    testRatings = get_test_ratings()

    methods, attack_statuses = scanFiles()
    rmse = get_all_rmse(testRatings, methods, attack_statuses)
    predRanks = getAllPredictionRanks(methods, attack_statuses, attackData["attacked items"])
    results = {}
    summary = {}

    # Add RMSE and pred ranks to results
    for method in methods:
        results[method] = {}
        results[method]["rmse"] = rmse[method]
        results[method]["predRanks"] = predRanks[method]
        results[method]["predShift"] = predShift(predRanks[method])
        results[method]["avgShift"] = avgShift(results[method]["predShift"])
        results[method]["hitRatio"] = {}
        for k in topk:
            results[method]["hitRatio"][k] = hitRatio(predRanks[method], k)

    for method in methods:
        summary[method] = {}
        summary[method]["rmse"] = results[method]["rmse"]
        summary[method]["avgShift"] = results[method]["avgShift"]
        summary[method]["hitRatio"] = results[method]["hitRatio"]

    for method in methods:
        output = {
            "attackData": attackData,
            "quickSummary": summary[method],
            "results": results[method]
        }
        with open(method + "." + output_suffix, "w") as outFile:
            outFile.write(json.dumps(output, indent=4, sort_keys=True))

if __name__=="__main__":
    main()
