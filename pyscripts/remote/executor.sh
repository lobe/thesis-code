#!/bin/bash

# Run against a heirachy of solutions and attack files
# Attack files are in form *.base (ratings.base for the attacked, unaltered.base for the original)
# FIRST ARG: The directory containing attack files
# Second Arg: The solutions.txt file in the base of the solutions directory (each line in that file is a different recommender program)

# TODO:
# - Add process pooling (to take advantage of that sexy multicore goodness)
# - Add logging, both affirmative (showing what ran) and negative (What failed)
# - Upload to S3 upon completion
# - Upload each result to S3, tag with commit, recommender hash and input file hash. Use for caching
# - Retrieve from S3 cache

ATTACK_DIR=$1
SOLUTION_FILE=`readlink -f "$2"`
SOLUTION_DIR=`dirname $SOLUTION_FILE`
ANALYSIS_PROGRAM=`readlink -f $3`
AGGREGATE_ANALYSIS_PROGRAM=`readlink -f "$4"`
HOME_DIR=`readlink -f "$5"`
COMMAND_POOL_FILE="commands.tmp"
RUNNING_FILE="executing.log"
NUM_CORES=`nproc`
S3_BUCKET="thesis-hashes"

function attack_checksum()
{
    CHECKSUM_FILES=`find . -type f -name "ratings.test" -o -name "attack.meta" -o -name "attacked.base" -o -name "unaltered.base"`
    echo "checksum calculated in $(pwd)"
    for check_file in $CHECKSUM_FILES
    do
        CHECKSUM=`md5sum $check_file`
        echo "checksum: $CHECKSUM"
    done
    ATTACK_CHECKSUM=`find . -type f -name "ratings.test" -o -name "attack.meta" -o -name "attacked.base" -o -name "unaltered.base" | xargs cat | md5sum | cut -d" " -f 1`
}

function solution_checksum()
{
    SOLUTION_CHECKSUM=`md5sum $SOLUTION_DIR/$1/bin/$1 | cut -d" " -f 1`
}
cd $HOME_DIR
touch $COMMAND_POOL_FILE # Ensure file exists before deletion as to not raise error
rm $COMMAND_POOL_FILE

INPUT_FILES=`find $ATTACK_DIR -type f | grep "attacked.base\|unaltered.base"`
DEEP_ATTACK_DIRS=`dirname $(find $ATTACK_DIR -type f | grep "attack.meta")`
mapfile -t ACTIVE_SOLUTIONS < $SOLUTION_FILE

for DEEP_ATTACK_DIR in $DEEP_ATTACK_DIRS
do
    cd $DEEP_ATTACK_DIR
    attack_checksum
    for inFile in "attacked.base" "unaltered.base"
    do
        cd $HOME_DIR
        FILE_BASE=`echo $inFile | cut -d "." -f 1`
        FILE_EXTENSION=`echo $inFile | cut -d "." -f 2`
        for SOLUTION in "${ACTIVE_SOLUTIONS[@]}"
        do
            solution_checksum $SOLUTION
            ANALYSIS_S3_FILE="$ATTACK_CHECKSUM/$SOLUTION_CHECKSUM.analysis"
            echo "(cd $DEEP_ATTACK_DIR && test -n \"\$(aws s3 ls s3://$S3_BUCKET/$ANALYSIS_S3_FILE)\" && aws s3 cp s3://$S3_BUCKET/$ANALYSIS_S3_FILE $SOLUTION.analysis) || (cd $HOME_DIR && $SOLUTION_DIR/$SOLUTION/bin/$SOLUTION $DEEP_ATTACK_DIR/$inFile > $DEEP_ATTACK_DIR/$FILE_BASE.$SOLUTION)" >> $COMMAND_POOL_FILE
        done
    done
done

# Now execute then cleanup
# See http://coldattic.info/shvedsky/pro/blogs/a-foo-walks-into-a-bar/posts/7 for escaping and threadpooling via xargs
cd $HOME_DIR
cat $COMMAND_POOL_FILE | while read i; do printf "%q\n" "$i"; done | tee $RUNNING_FILE | xargs -I CMD --max-procs=$NUM_CORES bash -c CMD
rm $COMMAND_POOL_FILE

META_FILES=`find $ATTACK_DIR -type f | grep "attack.meta"`
for metaFile in $META_FILES
do
    ATTACK_LOCATION=`dirname $metaFile`
#    cd $ATTACK_LOCATION
#    echo "Executing Analysis in $ATTACK_LOCATION"
    cd $HOME_DIR
    echo "cd $ATTACK_LOCATION && $ANALYSIS_PROGRAM" >> $COMMAND_POOL_FILE
done

cd $HOME_DIR
cat $COMMAND_POOL_FILE | while read i; do printf "%q\n" "$i"; done | xargs -I CMD --max-procs=$NUM_CORES bash -c CMD
rm $COMMAND_POOL_FILE

cd $HOME_DIR
# Now cache all those generated analysis files up in S3
ANALYSIS_FILES=`find $ATTACK_DIR -type f | grep -E "\.analysis$"`
for ANALYSIS in $ANALYSIS_FILES
do
    ANALYSIS_DIR=`dirname $(readlink -f "$ANALYSIS")`
    cd $ANALYSIS_DIR
    SOLUTION=`echo $ANALYSIS | rev | cut -d/ -f1 | rev | cut -d. -f1`
    attack_checksum
    solution_checksum $SOLUTION
    ANALYSIS_S3_FILE="$ATTACK_CHECKSUM/$SOLUTION_CHECKSUM.analysis"
    cd $HOME_DIR
    echo "cd $ANALYSIS_DIR && aws s3 cp $SOLUTION.analysis s3://$S3_BUCKET/$ANALYSIS_S3_FILE" >> $COMMAND_POOL_FILE
done

cd $HOME_DIR
cat $COMMAND_POOL_FILE | while read i; do printf "%q\n" "$i"; done | xargs -I CMD --max-procs=$NUM_CORES bash -c CMD
rm $COMMAND_POOL_FILE

cd $HOME_DIR
$AGGREGATE_ANALYSIS_PROGRAM $ATTACK_DIR