#!/usr/bin/python

import fileinput
from sets import Set
import sys

entries = {}

for line in fileinput.input():
    user, movie = line.split()[:2]
    try:
        entries[user].add(movie)
    except KeyError:
        entries[user] = Set([movie])

for line in sys.stdin:
    if not line.strip():
        continue
    user, movie = line.split()[:2]
    if user in entries:
        if movie in entries[user]:
            print(line.strip())
