#!/usr/bin/python3

import subprocess
import sys
from paramiko import SSHClient
import paramiko
import os

# Create a program that will setup all the files necessary for a successful execution on a set of tests
# The aim is to be able to set up the program to run distinct trials
# Every trial will have the commit # in the root dir
# That way each trial can run indepenently, only use resources required

DEVMODE = True

class TerminalClient:

    sshClient = None

    def __init__(self, host=None, user=None):
        if host is not None and user is not None:
            self.sshClient = SSHClient()
            self.sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.sshClient.connect(host, username=user)
        elif host is not None or user is not None:
            raise ValueError("user and host must either both be set, or neither")

    # Call and return exit code
    def call(self, command):
        if self.sshClient is None:
            return subprocess.call(command, shell=True)
        else:
            stdin, stdout, stderr = self.sshClient.exec_command(command)
            return stdout.channel.recv_exit_status()

    # Call and return stdout
    def check_output(self, command):
        if self.sshClient is None:
            return subprocess.check_output(command, shell=True)
        else:
            stdin, stdout, stderr = self.sshClient.exec_command(command)
            return stdout.read().decode("utf-8")



def main():
    print("========== EXECUTING RECOMMENDATION SETUP ENGINE ==============")
    print("========== Copies Attacks and solutions, makes binaries =======")

    ## BEGIN GENERAL CONFIG
    os.chdir(os.path.dirname(__file__))

    attackDir = "../attacks/"  # Where the attack files are located. Always on localhost
    solutionDir = "../solutions/"  # Where the recommenders are located. Always on localhost
    remoteProgramsDir = "remote/"  # Where the output filter is located. Always on localhost
    executorScript = "executor.sh"
    analysisProgram = "analysis.py"
    analysisAggregatorProgram = "aggregateAnalysis.py"
    executorLocal = remoteProgramsDir + "/executor.sh"


    ## BEGIN REMOTE HOST CONFIG

    trialDir = "/home/hamish/thesis/trials"  # Directory where Tests should be run. Maybe on a different server
    remoteUser = None
    remoteHost = None

    # remoteHost = "52.25.113.19"  # Server where tests should be run.
    # trialDir = "/home/ubuntu/trials"  # Directory where Tests should be run. Maybe on a different server
    # remoteUser = "ubuntu"

    # Check all is committed,
    if not DEVMODE and subprocess.call("git diff --exit-code > /dev/null", shell=True):
        print("Uncommitted changes. All changes must be committed before proceeding", file=sys.stderr)
        sys.exit(1)
    if not DEVMODE and subprocess.call("git ls-files --other --exclude-standard | sed q1 > /dev/null", shell=True):
        print("Untracked files not committed. All changes must be committed before proceeding", file=sys.stderr)
        sys.exit(1)

    # Check for commit hash
    commitHash = subprocess.check_output("git rev-parse HEAD", shell=True).decode("utf-8").strip()
    destinationRoot = trialDir + "/" + commitHash
    print("last commit was " + commitHash)

    # Get local and remote clients. Note that remote may be this machine
    localTerminal = TerminalClient()
    if remoteHost is None:
        remoteTerminal = TerminalClient()
    else:
        remoteTerminal = TerminalClient(host=remoteHost, user=remoteUser)

    #Create remote Dir
    ensureDirExists(remoteTerminal, destinationRoot)

    # Copy attack dir over
    remoteAttackDir = destinationRoot + "/attack"
    ensureDirExists(remoteTerminal, trialDir)
    copyToRemote(localTerminal, attackDir, remoteAttackDir, destMachine=remoteHost, destUser=remoteUser)
    # Copy solution dir over
    remoteSolutionDir = destinationRoot + "/solutions"
    ensureDirExists(remoteTerminal, trialDir)
    copyToRemote(localTerminal, solutionDir, remoteSolutionDir, args="-a --exclude bin/", destMachine=remoteHost, destUser=remoteUser)
    # Make solutions
    print("=======MAKING SOLUTIONS ON REMOTE==========")
    print(remoteTerminal.check_output(remoteSolutionDir + "/makeall.sh"))
    print("===========================================")
    print("==========REMOTE MAKE COMPLETE=============")
    print("===========================================")

    # Copy over remote scripts
    copyToRemote(localTerminal, remoteProgramsDir, destinationRoot, destMachine=remoteHost, destUser=remoteUser)
    # Run execution script
    print("==========EXECUTOR STARTED=================")
    remoteTerminal.call("cd " + destinationRoot + " && ./" + executorScript + " " + remoteAttackDir + " " + remoteSolutionDir + "/solutions.txt ./" + analysisProgram + "  ./" + analysisAggregatorProgram + " " + destinationRoot + " 2>&1 | tee executor.log")
    print("==========EXECUTOR ENDED===================")


def ensureDirExists(client, dir):
    return client.call("mkdir -p " + dir)

def copyToRemote(localClient, localDir, destinationDir, args=None, destMachine=None, destUser=None):
    if args is None:
        args = "-a"
    if destMachine is not None and destUser is not None:
        targetDir= destUser + "@" + destMachine + ":" + destinationDir
    else:
        targetDir = destinationDir
    return localClient.call(" ".join(["rsync", args, localDir, targetDir]))


if __name__ == "__main__":
    main()
