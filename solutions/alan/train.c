#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>


#define MAX_INT_PLUS_ONE  2147483648.0

#define NUM_DIMS          2

#define WEIGHT_DECAY      0.9995
#define NUM_EPOCHS0   20000
#define NUM_EPOCHS1  500000

#define NUM_USERS       943
#define NUM_MOVIES     1682
#define NUM_RATINGS   80000


double random_gaussian();

double zero_mean(double a[],int na[],int num_items);

void decay_weights(double a[],int num_items);

void print_array(
                 char *name,
                 double *a[],
                 int min_index,
                 int max_index,
                 int num_items
                );

double scale[6] = {0.0,0.0,0.25,0.5,0.75,1.0};

int rating[3][NUM_RATINGS+1];

int main( int argc, char *argv[] )
{
  double *u[NUM_DIMS+1],*du[NUM_DIMS+1];
  double *x[NUM_DIMS+1],*dx[NUM_DIMS+1];
  int    *nu,*nx;
  double s0,ds0;
  double t,z,s,ds;
  double eta;
  double sum,cost;
  long seed=0;
  int rate,seconds;
  int ep;
  int i,j,k,r,p;

  p=1;
  while( p < argc ) {
    if( argv[p][1] == 'r' ) {
      seed = atoi(argv[p+1]);
      p += 2;
    }
  }

  if( seed == 0 ) {
    seed = time( NULL );
  }
  printf("seed=%ld\n",seed);
  srandom(seed);

  nu = (int *)malloc((NUM_USERS +1)*sizeof(int));
  nx = (int *)malloc((NUM_MOVIES+1)*sizeof(int));
  memset( nu,0,(NUM_USERS +1)*sizeof(int));
  memset( nx,0,(NUM_MOVIES+1)*sizeof(int));
  for( k=0; k <= NUM_DIMS; k++ ) {
     u[k] = (double *)malloc((NUM_USERS+1)*sizeof(double));
    du[k] = (double *)malloc((NUM_USERS+1)*sizeof(double));
    memset( u[k],0,(NUM_USERS+1)*sizeof(double));
  }
  for( k=0; k <= NUM_DIMS; k++ ) {
     x[k] = (double *)malloc((NUM_MOVIES+1)*sizeof(double));
    dx[k] = (double *)malloc((NUM_MOVIES+1)*sizeof(double));
    memset( x[k],0,(NUM_MOVIES+1)*sizeof(double));
  }
  for( r=1; r <= NUM_RATINGS; r++ ) {
    scanf("%d %d %d %d\n",&i,&j,&rate,&seconds);
    rating[0][r] = i;
    rating[1][r] = j;
    rating[2][r] = rate;
    nu[i]++;
    nx[j]++;
  }

  sum = 0.0;
  for( r=1; r <= NUM_RATINGS; r++ ) {
    sum += scale[rating[2][r]];
  }
  s0 = -log( -1.0 + NUM_RATINGS/sum);

  printf("s0 = %1.4f, sum = %f, ratings = %d;\n",s0, sum, NUM_RATINGS);

  printf("cost0 = [\n");

  // learn only s0,u0,x0
  eta = 0.01;
  for( ep=1; ep <= NUM_EPOCHS0; ep++ ) {
    ds0 = 0.0;
    memset(du[0],0,(NUM_USERS +1)*sizeof(double));
    memset(dx[0],0,(NUM_MOVIES+1)*sizeof(double));
    cost = 0.0;
    for( r=1; r <= NUM_RATINGS; r++ ) {
      i = rating[0][r]; // What user
      j = rating[1][r]; // What movie
      t = scale[rating[2][r]]; // The scaled rating user i gave to movie j
      s = s0 + u[0][i] + x[0][j]; // Combination of standard offset + user offset + movie offset (to feed into g(s))
      z = 1.0/(1.0 + exp(-s)); // g(s)
      ds= z *( 1 - z )*( t - z ); // dg/ds
      cost = cost + ( t - z )*( t - z ); // Update the cost to add the error squared between actual rating and estimated rating
      ds0 += ds;
      du[0][i] += ds; // set du[0][i] to be Partial derivate du/ds
      dx[0][j] += ds; // set dx[0][i] to be Partial derivative dx/ds
    }
    for( i=1; i <= NUM_USERS; i++ ) {
      u[0][i] += eta * du[0][i]; // Update each user's bias to go down the cost function, by learning rate
    }
    for( j=1; j <= NUM_MOVIES; j++ ) {
      x[0][j] += eta * dx[0][j]; // Update each movie's bias to go down the cost function, scaled by learning rate
    }
    double uMean = zero_mean(u[0],nu,NUM_USERS); // Update s0 to 
    // int i;
    // double sum = 0.0;
    // for (i = 1; i <= NUM_USERS ; i++)
    //   printf("u[%d] is %1.10f\n", i, u[0][i]);
    // for (i = 1; i <= NUM_USERS ; i++)
    //   sum += u[0][i];
    // printf("sum is %1.10f\n", sum);
    // printf("Iteration %d and uMean is %1.10f", ep, uMean);
    s0 += uMean;
    s0 += zero_mean(x[0],nx,NUM_MOVIES);

    decay_weights(u[0],NUM_USERS);
    decay_weights(x[0],NUM_MOVIES);

    if( ep % 1000 == 0 ) {
      printf(" %1.4f %1.2f\n",s0,cost);
      fflush(stdout);
    }
  }

  printf("];\n");
  printf("s0 = %1.4f;\n",s0);

  print_array("u0",u,0,0,NUM_USERS);
  print_array("x0",x,0,0,NUM_MOVIES);

  printf("cost = [\n");

  for( k=1; k <= NUM_DIMS; k++ ) {
    for( i=1; i <= NUM_USERS; i++ ) {
      if( nu[i] > 1 ) {
	u[k][i] = 0.0001 * random_gaussian();
      }
    }
    for( j=1; j <= NUM_MOVIES; j++ ) {
      if( nx[j] > 1 ) {
	x[k][j] = 0.0001 * random_gaussian();
      }
    }
  }
  // learn s0,u0,x0,u,x
  eta = 0.01;
  for( ep=1; ep <= NUM_EPOCHS1; ep++ ) {
    ds0 = 0.0;
    for( k=0; k <= NUM_DIMS; k++ ) {
      memset(du[k],0,(NUM_USERS +1)*sizeof(double));
      memset(dx[k],0,(NUM_MOVIES+1)*sizeof(double));
    }
    double lastCost = cost;
    cost = 0.0;
    for( r=1; r <= NUM_RATINGS; r++ ) {
      i = rating[0][r];
      j = rating[1][r];
      t = scale[rating[2][r]];
      s = s0 + u[0][i] + x[0][j];
      for( k=1; k <= NUM_DIMS; k++ ) {
        s += u[k][i]*x[k][j];
      }
      z = 1.0/(1.0 + exp(-s));
      ds = z *( 1 - z )*( t - z );
      cost = cost + ( t - z )*( t - z );
      ds0 += ds;
      du[0][i] += ds;
      dx[0][j] += ds;
      for( k=1; k <= NUM_DIMS; k++ ) {
        du[k][i] += ds * x[k][j];
        dx[k][j] += ds * u[k][i];
      }
    }
    for( k=0; k <= NUM_DIMS; k++ ) {
      for( i=1; i <= NUM_USERS; i++ ) {
        u[k][i] += eta * du[k][i];
      }
      for( j=1; j <= NUM_MOVIES; j++ ) {
        x[k][j] += eta * dx[k][j];
      }
    }
    s0 += zero_mean(u[0],nu,NUM_USERS);
    s0 += zero_mean(x[0],nx,NUM_MOVIES);
    decay_weights(u[0],NUM_USERS);
    decay_weights(x[0],NUM_MOVIES);
    for( k=1; k <= NUM_DIMS; k++ ) {
      zero_mean(u[k],nu,NUM_USERS);
      zero_mean(x[k],nx,NUM_MOVIES);
      decay_weights(u[k],NUM_USERS);
      decay_weights(x[k],NUM_MOVIES);
    }
    if( ep % 1000 == 0 ) {
      printf(" %1.4f %1.2f %f\n",s0,cost, lastCost - cost);
      fflush(stdout);
    }
  }

  printf("];\n");
  printf("s0 = %1.4f;\n",s0);

  print_array("u",u,0,NUM_DIMS,NUM_USERS);
  print_array("x",x,0,NUM_DIMS,NUM_MOVIES);

  return 0;
}

/********************************************************//**
   Return random variable from a Gaussian distribution
*/
double random_gaussian()
{
  static double V1, V2, S;
  static int phase = 0;
  double X;
  if(phase == 0) {
    do {
      double U1 =  random() / MAX_INT_PLUS_ONE;
      double U2 =  random() / MAX_INT_PLUS_ONE;
      V1 = 2.0 * U1 - 1.0;
      V2 = 2.0 * U2 - 1.0;
      S = V1 * V1 + V2 * V2;
    } while( S >= 1.0 || S == 0.0 );

    X = V1 * sqrt(-2.0 * log(S) / S);
  }
  else {
    X = V2 * sqrt(-2.0 * log(S) / S);
  }
  phase = 1 - phase;

  return X;
}

double zero_mean(double a[],int na[],int num_items)
{
  double sum = 0.0;
  double mean;
  int count=0;
  int i;
  for( i=1; i <= num_items; i++ ) {
    if( na[i] > 0 ) {
      sum += a[i];
      count++;
    }
    else if( a[i] != 0.0 ) {
      printf("HEY!\n");
    }
  }
  mean = sum/(double)count;
  for( i=1; i <= num_items; i++ ) {
    if( na[i] > 0 ) {
      a[i] -= mean;
    }
  }
  return( mean );
}

void decay_weights(double a[],int num_items)
{
  int i;
  for( i=1; i <= num_items; i++ ) {
    a[i] *= WEIGHT_DECAY;
  }
}

void print_array(
                 char *name,
                 double *a[],
                 int min_index,
                 int max_index,
                 int num_items
                )
{
  int i,j;
  printf("%s = [\n",name);
  for( j=1; j <= num_items; j++ ) {
    for( i = min_index; i <= max_index; i++ ) {
      printf(" %7.4f",a[i][j]);
    }
    printf("\n");
  }
  printf("];\n");
}
