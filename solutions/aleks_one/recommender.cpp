#include "ratings.h"
#include "aleks/aleks.h"
#include <vector>
#include <map>

using namespace std;

RatingCollection predictRatings(RatingCollection rc)
{
	rating_matrix inRatings = collectionToMatrix(rc);
	auto cred = aleksAlgo(inRatings, 2.0);

	auto trust = cred.first;
	auto credibility = cred.second;

	double averageTrust = 0.0;
	unsigned int trustCount = 0;
	double badTrust = 0.0;
	unsigned int badCount = 0;
	for (auto it = trust.begin(); it != trust.end(); ++it)
	{
		averageTrust += it->second;
		++trustCount;
		if (it->first >= 943)
		{
			badTrust += it->second;
			++badCount;
		}
	}
	averageTrust /= trustCount;
	badTrust /= badCount;

	// Collect all the bad trust into a vector for easy debugging
	map<unsigned int, double> badTrustValues;
	for (auto it = trust.begin(); it != trust.end(); ++it)
	{
		if (it->first >= 943)
		{
			badTrustValues.insert(*it);
		}
	}

	// unsigned int attackedItem = 267;
	// auto attackedCred = credibility.at(attackedItem);
	// unsigned int attackedItemVotes = 0;
	// unsigned int colludedVotes = 0;
	// for (auto r: rc.ratings)
	// {
	// 	if (r.movie == attackedItem)
	// 	{
	// 		if (r.user >= 943)
	// 			++colludedVotes;
	// 		++attackedItemVotes;
	// 	}
	// }
	// cout << "Colluded votes are " << colludedVotes << "/" << attackedItemVotes << " = " << (double)((colludedVotes / attackedItemVotes) * 100) << endl;

	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	double defaultRating = 0.4; // Uniform rating for everyone
	for (unsigned int u = 0; u < rc.numUsers; ++u) {
		for (unsigned int m = 0; m < rc.numMovies; ++m) {
			Rating rating;
			rating.user = u;
			rating.movie = m;
			rating.rating = defaultRating;
			res.ratings.push_back(rating);
		}
	}

	return res;
}