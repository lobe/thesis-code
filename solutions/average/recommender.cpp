#include "ratings.h"
#include <vector>
#include <cfenv>
#include <algorithm>

using namespace std;

double averageRating(const vector<Rating>& v);

RatingCollection predictRatings(RatingCollection rc)
{
	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	double defaultRating = averageRating(rc.ratings);
	for (unsigned int u = 0; u < rc.numUsers; ++u) {
		for (unsigned int m = 0; m < rc.numMovies; ++m) {
			Rating rating;
			rating.user = u;
			rating.movie = m;
			rating.rating = defaultRating;
			res.ratings.push_back(rating);
		}
	}

	return res;
}

double averageRating(const vector<Rating>& v)
{
	double sum = accumulate(v.begin(), v.end(), 0.0,
		[&](double sum, const Rating& curr) { return sum + curr.rating; });
	return sum / v.size();
}