#include "ratings.h"
#include "float.h"
#include <vector>
#include <cfenv>
#include <algorithm>
#include "gpu/reduction.h"
#include <random>

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#define  MAX_INT_PLUS_ONE  2147483648.0

using namespace std;

double averageRating(const vector<Rating>& v);
double average(const vector<double>& a);
double zeroMean(vector<double>& v);
double sigmoid(double x);
void weightDecay(vector<double>& v, double decayRate);
double bin_prob(double z, unsigned int rating);
void applyNoise(vector<double>& v);
double random_gaussian();
double fullReduce(unsigned int size, double *toReduce, double *gpu_temp, double *temp);

__device__ double atomicAdd(double* address, double val)
{
    unsigned long long int* address_as_ull =
                                          (unsigned long long int*)address;
    unsigned long long int old = *address_as_ull, assumed;
    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed, 
                        __double_as_longlong(val + 
                        __longlong_as_double(assumed)));
    } while (assumed != old);
    return __longlong_as_double(old);
}

__device__ 
double bin_prob_device(double z, unsigned int rating)
{
	double prob = 0.0;
	switch( rating ) {
		case 1: prob =   (1-z)*(1-z)*(1-z)*(1-z); break;
		case 2: prob = 4*(1-z)*(1-z)*(1-z)* z; break;
		case 3: prob = 6*(1-z)*(1-z)*  z  * z; break;
		case 4: prob = 4*(1-z)*  z  *  z  * z; break;
		case 5: prob =     z  *  z  *  z  * z; break;
	}

	return prob;
}

__device__
inline unsigned int unscaleRatingDevice(double rating)
{
	return 4*(rating + 0.25);
}

__global__
void applyDerivative(double* values, double* derivatives, unsigned int numElements, double learningRate, double lambda, double minValue)
{
     for (unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; i < numElements; i += blockDim.x * gridDim.x) {
		values[i] = max(minValue, values[i] + learningRate * (derivatives[i] - lambda * values[i]));
    }   
}

__global__
void first_stage_descent(unsigned int numRatings, Rating* ratings, double s0,
						 unsigned int numUsers, unsigned int numMovies,
						 double* gpu_u, double* gpu_v, double* gpu_x,
						 double* gpu_du, double* gpu_dv, double* gpu_dx,
						 double* interim_ds)
{
	double s;
	double z;
	double ds;
	Rating curr;

    for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < numRatings; i += blockDim.x * gridDim.x) {
		curr = ratings[i];
		s = s0 + gpu_v[curr.user] + (1.0 + gpu_u[curr.user]) * gpu_x[curr.movie];
		z = 1.0/(1.0 + exp(-s));
		ds= curr.rating - z;

		ds *= bin_prob_device(z, unscaleRatingDevice(curr.rating));

		interim_ds[i] += ds;
		atomicAdd(gpu_dv + curr.user, ds);
		atomicAdd(gpu_du + curr.user, ds * gpu_x[curr.movie]);
		atomicAdd(gpu_dx + curr.movie, ds * (1.0 + gpu_u[curr.user]));
	} 

}

// __global__
// void first_stage_descent(unsigned int numRatings, Rating* ratings, double s0,
// 						 unsigned int numUsers, unsigned int numMovies,
// 						 double* gpu_u, double* gpu_v, double* gpu_x,
// 						 double* gpu_du, double* gpu_dv, double* gpu_dx,
// 						 double* interim_cost, double* interim_prob, double* interim_ds)
// {
// 	double s;
// 	double z;
// 	double ds;
// 	double curr_prob;
// 	Rating curr;

//     for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < numRatings; i += blockDim.x * gridDim.x) {
// 		curr = ratings[i];
// 		s = s0 + gpu_v[curr.user] + (1.0 + gpu_u[curr.user]) * gpu_x[curr.movie];
// 		z = 1.0/(1.0 + exp(-s));
// 		ds= curr.rating - z;


// 		curr_prob = bin_prob_device(z, unscaleRatingDevice(curr.rating));
// 		ds *= curr_prob;

// 		interim_prob[i] += curr_prob;
// 		interim_cost[i] += (curr.rating - z) * (curr.rating - z);
// 		interim_ds[i] += ds;
// 		atomicAdd(gpu_dv + curr.user, ds);
// 		atomicAdd(gpu_du + curr.user, ds * gpu_x[curr.movie]);
// 		atomicAdd(gpu_dx + curr.movie, ds * (1.0 + gpu_u[curr.user]));
// 	} 

// }

RatingCollection predictRatings(RatingCollection rc)
{
	// Shuffle to ensure randomness
	random_shuffle(rc.ratings.begin(), rc.ratings.end());

	double avRating = averageRating(rc.ratings);
	double s0 = -log(1.0/avRating - 1);

	double *u, *x, *v; 
	// double *du, *dx, *dv;
	double *gpu_u, *gpu_x, *gpu_v, *gpu_du, *gpu_dx, *gpu_dv;
	double *gpu_dsRed;
	// double *gpu_costRed, *gpu_probRed; // Memory for reducing
	double *interim_ds;
	// double *interim_cost, *interim_prob;
	double *dsRed;
	// double *costRed, *probRed;
	Rating *ratings, *gpu_ratings;

	// Allocate u, v, w, du, dv, dw on host
	u = (double*)malloc(rc.numUsers * sizeof(double));
	v = (double*)malloc(rc.numUsers * sizeof(double));
	x = (double*)malloc(rc.numMovies * sizeof(double));
	// du = (double*)malloc(rc.numUsers * sizeof(double));
	// dv = (double*)malloc(rc.numUsers * sizeof(double));
	// dx = (double*)malloc(rc.numMovies * sizeof(double));	

	// *Red only needs 1/512 numRatings, but may as well give more
	// In case of edge case bugs
	dsRed = (double*)malloc(rc.ratings.size() * sizeof(double));
	// costRed = (double*)malloc(rc.ratings.size() * sizeof(double));
	// probRed = (double*)malloc(rc.ratings.size() * sizeof(double));				
	ratings = (Rating*)malloc(rc.ratings.size() * sizeof(Rating));
	// Initialise variables on host
	memset(u, 0.0, rc.numUsers * sizeof(double));
	memset(v, 0.0, rc.numUsers * sizeof(double));
	memset(x, 0.0, rc.numMovies * sizeof(double));
	for (unsigned int i = 0; i < rc.ratings.size(); ++i)
		ratings[i] = rc.ratings[i];

	// Allocate on gpu
	cudaError_t u_err = cudaMalloc((void **) &gpu_u, rc.numUsers * sizeof(double));
	cudaError_t v_err = cudaMalloc((void **) &gpu_v, rc.numUsers * sizeof(double));
	cudaError_t x_err = cudaMalloc((void **) &gpu_x, rc.numMovies * sizeof(double));
	cudaError_t du_err = cudaMalloc((void **) &gpu_du, rc.numUsers * sizeof(double));
	cudaError_t dv_err = cudaMalloc((void **) &gpu_dv, rc.numUsers * sizeof(double));
	cudaError_t dx_err = cudaMalloc((void **) &gpu_dx, rc.numMovies * sizeof(double));
	cudaError_t dsRed_err = cudaMalloc((void **) &gpu_dsRed, rc.ratings.size() * sizeof(double));
	// cudaError_t costRed_err = cudaMalloc((void **) &gpu_costRed, rc.ratings.size() * sizeof(double));
	// cudaError_t probRed_err = cudaMalloc((void **) &gpu_probRed, rc.ratings.size() * sizeof(double));
	cudaError_t ratings_err = cudaMalloc((void **) &gpu_ratings, rc.ratings.size() * sizeof(Rating));						

	cudaError_t ds_err = cudaMalloc((void **) &interim_ds, rc.ratings.size() * sizeof(double));
	// cudaError_t prob_err = cudaMalloc((void **) &interim_prob, rc.ratings.size() * sizeof(double));
	// cudaError_t cost_err = cudaMalloc((void **) &interim_cost, rc.ratings.size() * sizeof(double));

	if (u_err != cudaSuccess || v_err != cudaSuccess || x_err != cudaSuccess || 
		dx_err != cudaSuccess || du_err != cudaSuccess || dv_err != cudaSuccess || 
		dsRed_err != cudaSuccess || ds_err != cudaSuccess ||
		// costRed_err != cudaSuccess || probRed_err != cudaSuccess ||
		// || prob_err != cudaSuccess || cost_err != cudaSuccess
		ratings_err != cudaSuccess)
	{
		cerr << "MEMORY FAILURE" << endl;
		exit(1);
	}

	cudaMemset(gpu_u, 0.0, rc.numUsers * sizeof(double));
	cudaMemset(gpu_v, 0.0, rc.numUsers * sizeof(double));		
	cudaMemset(gpu_x, 0.0, rc.numMovies * sizeof(double));
	ratings_err = cudaMemcpy(gpu_ratings, ratings, rc.ratings.size() * sizeof(Rating), cudaMemcpyHostToDevice);	

	if (ratings_err != cudaSuccess)
	{
		cerr << "MEMORY COPY FAILURE" << endl;
		exit(1);
	}

	double cost = 0.0;
	double lastCost = 1000.0; // Anything but 0.0;
	double prob = 0.0;
	double lastProb = 1000.0;	
	double learningRate = 0.001;
	unsigned int iterations = 0;
	unsigned int maxIterations = 20000;
	// unsigned int maxIterations = 1000;
	double epsilon = 0.000001;
	double lambda0 = 0.1;
	double lambda1 = 1.0;
	// double decayRate = 0.9995;

	while((abs(cost - lastCost) > epsilon || abs(prob - lastProb) > epsilon) && iterations < maxIterations)
	{
		double ds0 = 0.0;
		cudaMemset(gpu_du, 0.0, rc.numUsers * sizeof(double));
		cudaMemset(gpu_dv, 0.0, rc.numUsers * sizeof(double));		
		cudaMemset(gpu_dx, 0.0, rc.numMovies * sizeof(double));
		cudaMemset(gpu_dsRed, 0.0, rc.ratings.size() * sizeof(double));
		cudaMemset(interim_ds, 0.0, rc.ratings.size() * sizeof(double));		
		// cudaMemset(gpu_costRed, 0.0, rc.ratings.size() * sizeof(double));		
		// cudaMemset(gpu_probRed, 0.0, rc.ratings.size() * sizeof(double));		
		// cudaMemset(interim_cost, 0.0, rc.ratings.size() * sizeof(double));
		// cudaMemset(interim_prob, 0.0, rc.ratings.size() * sizeof(double));
		lastCost = cost + 100;
		lastProb = prob + 100;
		prob = 0.0;
		cost = 0.0; // Only used for debugging / progress checks

		// first_stage_descent<<<(rc.ratings.size()+255)/256,256>>>(rc.ratings.size(), gpu_ratings, s0,
		// 													 rc.numUsers, rc.numMovies,
		// 													 gpu_u, gpu_v, gpu_x,
		// 													 gpu_du, gpu_dv, gpu_dx,
		// 													 interim_cost, interim_prob, interim_ds);
		first_stage_descent<<<(rc.ratings.size()+255)/256,256>>>(rc.ratings.size(), gpu_ratings, s0,
															 rc.numUsers, rc.numMovies,
															 gpu_u, gpu_v, gpu_x,
															 gpu_du, gpu_dv, gpu_dx,
															 interim_ds);

		ds0 = fullReduce(rc.ratings.size(), interim_ds, gpu_dsRed, dsRed);
		// cost = fullReduce(rc.ratings.size(), interim_cost, gpu_costRed, costRed);
		// prob = fullReduce(rc.ratings.size(), interim_prob, gpu_probRed, probRed);

		s0 += 0.01 * learningRate * ds0;

		applyDerivative<<<(rc.numUsers + 255)/256, 256>>>(gpu_v, gpu_dv, rc.numUsers, learningRate, lambda0, -DBL_MAX);
		applyDerivative<<<(rc.numUsers + 255)/256, 256>>>(gpu_u, gpu_du, rc.numUsers, learningRate, lambda1, -1.0);
		applyDerivative<<<(rc.numMovies + 255)/256, 256>>>(gpu_x, gpu_dx, rc.numMovies, learningRate, lambda0, -DBL_MAX);

		++iterations;
		// if (iterations % 1000 == 0)
		// {
		// 	cout << iterations << endl;
		// 	cout << "    " << cost << " " << lastCost << " " << abs(cost - lastCost) << endl;
		// 	cout << "    " << prob << " " << lastProb << " " << abs(prob - lastProb) << endl;
		// }		
	}

	// Now stage 2 - use crossproduct parameters (that link movies with users)
	// unsigned int numDimensions = 2;
	cost = 0.0;
	lastCost = 1000.0;
 	prob = 0.0;
	lastProb = 1000.0;	
	learningRate = 0.0001;
	iterations = 0;
	maxIterations = 1000000;

	// How many cross products to calculate??
	// unsigned int numCrossProducts = 6;

	// // Now need a multidimensional datastructure to hold several u and x. Set bias for sets 0, initialse all others to 0
	// auto uBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numUsers));
	// auto xBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numMovies));
	// auto duBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numUsers)); // Plus one as a need for the base bias
	// auto dxBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numMovies)); // Plus one as a need for the base bias
	// // Check later -- does this cause a memory leak? as original vector not destroyed??
	// uBias.at(0) = u;
	// xBias.at(0) = x;
	// duBias.at(0) = du;
	// dxBias.at(0) = du;

	// // Seed u(1..numProducts) and x(1..numProducts) with some random noise to kickstart (but only for cross products)
	// for (auto k = uBias.begin() + 1; k != uBias.end(); ++k)
	// {
	// 	applyNoise(*k);
	// }
	// for (auto k = xBias.begin() + 1; k != xBias.end(); ++k)
	// {
	// 	applyNoise(*k);
	// }

	// // for (unsigned int i = 1; i < numCrossProducts + 1; ++i)
	// // {
	// // 	for (auto iter = uBias[i].begin(); iter != uBias[i].end(); ++iter)
	// // 		*iter = 0.001 * i;
	// // 	for (auto iter = xBias[i].begin(); iter != xBias[i].end(); ++iter)
	// // 		*iter = 0.001 * i;		
	// // }

	// while((abs(cost - lastCost) > epsilon || abs(prob - lastProb) > epsilon) && iterations < maxIterations)
	// {
	// 	double ds0 = 0.0;
	// 	for (unsigned int i = 0; i < numCrossProducts + 1; ++i)
	// 	{
	// 		fill(duBias[i].begin(), duBias[i].end(), 0);
	// 		fill(dxBias[i].begin(), dxBias[i].end(), 0);
	// 	}
	// 	fill(dv.begin(), dv.end(), 0);
	// 	fill(du.begin(), du.end(), 0);
	// 	fill(dx.begin(), dx.end(), 0);
	// 	lastCost = cost;
	// 	lastProb = prob;
	// 	prob = 0.0;
	// 	cost = 0.0; // Only used for debugging / progress checks

	// 	// Loop through all the ratings
	// 	for (auto r : rc.ratings)
	// 	{
	// 		double s = s0 + v[r.user] + (1.0 + u[r.user]) * x[r.movie];
	// 		for (unsigned int i = 1; i < numCrossProducts + 1; ++i)
	// 		{
	// 			s += uBias[i][r.user] * xBias[i][r.movie];
	// 		}
	// 		double z = sigmoid(s);
	// 		double ds = r.rating - z;
	// 		// double ds = z *( 1 - z ) * ( r.rating - z );

	// 		ds *= bin_prob(z, unscaleRating(r.rating));
	// 		prob += bin_prob(z, unscaleRating(r.rating));

	// 		cost += (r.rating - z) * ( r.rating - z);

	// 		ds0 += ds;
	// 		dv[r.user] += ds;
	// 		du[r.user] += ds * x[r.movie];
	// 		dx[r.movie] += ds * (1.0 + u[r.user]);
	// 		for (unsigned int i = 1; i < numCrossProducts + 1; ++i)
	// 		{
	// 			duBias[i][r.user] += ds * xBias[i][r.movie];
	// 			dxBias[i][r.movie] += ds * uBias[i][r.user];
	// 		}

	// 		// if (r.user == 0 && r.movie == 0)
	// 		// {
	// 		// 	cout << "old " <<  iterations + 1 << " u[" << 1 << "] " << u[0] << endl; 
	// 		// 	cout << "old " << iterations + 1 << " du[" << 1 << "] " << du[0] << endl; 
	// 		// }
	// 		// cout << iterations + 1 << " " << r.user << " " << r.movie << " " << ds << " " << du[r.user] << endl;
	// 	}

	// 	// if (iterations == 4)
	// 	// 	exit(1);

	// 	s0 += 0.01 * learningRate * ds0;

	// 	applyDerivative(v, dv, learningRate, lambda0);
	// 	applyDerivative(u, du, learningRate, lambda1, -1.0);
	// 	applyDerivative(x, dx, learningRate, lambda0);
	// 	for (unsigned int i = 1; i < numCrossProducts + 1; ++i)
	// 	{
	// 		// uBias and xBias should be same size (numCrossProducts + 1)
	// 		applyDerivative(uBias[i], duBias[i], learningRate, lambda1);
	// 		applyDerivative(xBias[i], dxBias[i], learningRate, lambda1);
	// 	}

	// 	// cout << "new " << iterations + 1 << " u[" << 1 << "] " << u[0] << endl << endl; 
	// 	// for (unsigned int i = 0; i < u.size(); ++i)
	// 	// 	cout << iterations + 1 << " u[" << i + 1 << "] " << u[i] << endl; 
	// 	// for (unsigned int i = 0; i < v.size(); ++i)
	// 	// 	cout << iterations + 1 << " v[" << i + 1 << "] " << v[i] << endl; 
	// 	// for (unsigned int i = 0; i < x.size(); ++i)
	// 	// 	cout << iterations + 1 << " x[" << i + 1 << "] " << x[i] << endl; 

	// 	++iterations;
	// 	// if (iterations % 100 == 0)
	// 	// {
	// 	// 	cout << iterations << endl;
	// 	// 	cout << "    " << cost << " " << lastCost << " " << abs(cost - lastCost) << endl;
	// 	// 	cout << "    " << prob << " " << lastProb << " " << abs(prob - lastProb) << endl;
	// 	// }
	// }

	// Copy v, u, x, back to the cpu RAM
	u_err = cudaMemcpy(u, gpu_u, rc.numUsers * sizeof(double), cudaMemcpyDeviceToHost);
	v_err = cudaMemcpy(v, gpu_v, rc.numUsers * sizeof(double), cudaMemcpyDeviceToHost);	
	x_err = cudaMemcpy(x, gpu_x, rc.numMovies * sizeof(double), cudaMemcpyDeviceToHost);

	// Now construct a Ratings Collection from these facts
	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	for (unsigned int user = 0; user < rc.numUsers; ++user) {
		for (unsigned int movie = 0; movie < rc.numMovies; ++movie) {
			Rating rating;
			rating.user = user;
			rating.movie = movie;
			double s = s0 + v[user] + (1 + u[user]) * x[movie];
			// for (unsigned int i = 1; i < numCrossProducts + 1; ++i)
			// {
			// 	s += uBias[i][user] * xBias[i][movie];
			// }
			rating.rating = sigmoid(s);
			res.ratings.push_back(rating);
		}
	}

	return res;
}

double fullReduce(unsigned int size, double *toReduce, double *gpu_temp, double *temp)
{
	reduce(size, 256, 256, 6, toReduce, gpu_temp);
	int tempsize = (size + 512) / 512;
	cudaMemcpy(temp, gpu_temp, tempsize * sizeof(double), cudaMemcpyDeviceToHost);	
	double returnSum = 0;
	for (auto i = 0; i < tempsize; ++i)
		returnSum += temp[i];
	return returnSum;
}

double averageRating(const vector<Rating>& v)
{
	double sum = accumulate(v.begin(), v.end(), 0.0,
		[&](double sum, const Rating& curr) { return sum + curr.rating; });
	return sum / v.size();
}

double average(const vector<double>& a)
{
	double sum = accumulate(a.begin(), a.end(), 0.0,
		[&](double sum, const double& curr) { return sum + curr; });
	return sum / a.size();
}

// This function relies on the fact that every user/movie is represented at least once (i.e. indexed contiguously)
double zeroMean(vector<double>& v)
{
	double av = average(v);
	transform(v.begin(), v.end(), v.begin(), [&](double& r){ return r -= av;});
	return av;
}

double sigmoid(double x)
{
	return 1.0 / (1.0 + exp(-x));
}

void weightDecay(vector<double>& v, double decayRate)
{
	transform(v.begin(), v.end(), v.begin(), [&](double& w){ return w *= decayRate; });
}

void applyNoise(vector<double>& v)
{
	transform(v.begin(), v.end(), v.begin(), [&](double& w){ return 0.1 * random_gaussian(); });
}

double bin_prob(double z, unsigned int rating)
{
	double prob = 0.0;
	switch( rating ) {
		case 1: prob =   (1-z)*(1-z)*(1-z)*(1-z); break;
		case 2: prob = 4*(1-z)*(1-z)*(1-z)* z; break;
		case 3: prob = 6*(1-z)*(1-z)*  z  * z; break;
		case 4: prob = 4*(1-z)*  z  *  z  * z; break;
		case 5: prob =     z  *  z  *  z  * z; break;

		default:
			cout << "SHOULD NEVER HAVE CALLED bin_prob with rating = " << rating << endl;
			exit(1);
	}

	return prob;
}

/********************************************************//**
   Return random variable from a Gaussian distribution
*/
double random_gaussian()
{
  static double V1, V2, S;
  static int phase = 0;
  double X;
  if(phase == 0) {
    do {
      double U1 =  random() / 2147483648.0;
      double U2 =  random() / 2147483648.0;
      V1 = 2.0 * U1 - 1.0;
      V2 = 2.0 * U2 - 1.0;
      S = V1 * V1 + V2 * V2;
    } while( S >= 1.0 || S == 0.0 );

    X = V1 * sqrt(-2.0 * log(S) / S);
  }
  else {
    X = V2 * sqrt(-2.0 * log(S) / S);
  }
  phase = 1 - phase;

  return X;
}