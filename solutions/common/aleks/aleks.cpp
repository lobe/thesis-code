#include "aleks.h"
#include <math.h>
#include <vector>
#include <map>
#include <algorithm>
#include <iostream>

using namespace std;

double nNorm(const vector<double>& input, int n);

pair<trust_type, credibility_type> aleksAlgo(rating_matrix& ratings, double alpha)
{
	double epsilon = 0.001;

	// Transform ratings to be item / rating based
	// Item -> Rating Given -> Vector of users giving that rating
	map<unsigned int, map<double, vector<unsigned int>>> givers;
	for (auto user = ratings.begin(); user != ratings.end(); ++user)
	{
		for (auto item = user->second.begin(); item != user->second.end(); ++item)
		{
			auto itemGiver = givers.find(item->first);
			if (itemGiver == givers.end()) {
				itemGiver = givers.insert(make_pair(item->first, map<double, vector<unsigned int>>())).first;
			}
			// Now find the entry for the rating
			auto ratingGiver = itemGiver->second.find(item->second.rating);
			if (ratingGiver == itemGiver->second.end()) {
				ratingGiver = itemGiver->second.insert(make_pair(item->second.rating, vector<unsigned int>())).first;
			}
			// Now insert the user to the rating entry
			ratingGiver->second.push_back(user->first);
		}
	}


	trust_type trust;
	for (auto votes = ratings.begin(); votes != ratings.end(); ++votes)
	{
		trust[votes->first] = 1.0;
	}
	// Calculate credibility
	// item -> rating given -> credibility
	credibility_type credibility;
	// Zero fill credibility (will be properly set in loop)
	for (auto itemIt = givers.begin(); itemIt != givers.end(); ++itemIt)
	{
		auto r = itemIt->second;
		credibility.insert(make_pair(itemIt->first, map<double, double>()));
		for (auto ratingIt = r.begin(); ratingIt != r.end(); ++ratingIt)
		{
			credibility.at(itemIt->first).insert(make_pair(ratingIt->first, 0));
		}
	}

	// Keep a vector of all delta's between crediblity values
	// in order to compute convergence and terminate when sum of deltas < epsilon
	vector<double> deltas;

	while (true)
	{
		deltas.clear();

		// Compute credibility

		// Now calculate credibility
		for (auto itemIt = credibility.begin(); itemIt != credibility.end(); ++itemIt)
		{
			double itemSum = 0.0;
			vector<double> itemDeltas;
			for (auto ratingIt = itemIt->second.begin(); ratingIt != itemIt->second.end(); ++ratingIt)
			{
				itemDeltas.push_back(ratingIt->second); // Save the trust level for the given rating
				double ratingSum = 0.0;
				ratingIt->second = 0.0;
				// Loop through all users that gave that item a given rating
				// and add their Trust to the score;
				auto users = givers.at(itemIt->first).at(ratingIt->first);
				for (auto userIt = users.begin(); userIt != users.end(); ++userIt)
				{
					ratingIt->second += pow(trust[*userIt], alpha);
					ratingSum += pow(trust[*userIt], alpha);
				}
				itemSum += pow(ratingSum, 2);
			}
			// Now divide every rating level by the root of the item sum. Compare with itemDelta and add to deltas
			itemSum = sqrt(itemSum);
			auto deltaIt = itemDeltas.begin();
			for (auto ratingIt = itemIt->second.begin(); ratingIt != itemIt->second.end(); ++ratingIt)
			{
				ratingIt->second /= itemSum;
				deltas.push_back(*deltaIt - ratingIt->second);
				++deltaIt;
			}
		}

		// Break if at convergence
		double difference = nNorm(deltas, 2);
		// cout << "Difference is " << difference << endl;
		if (difference < epsilon)
			break;

		// Compute trust ratings
		// First reset all trust to 0
		for (auto it = trust.begin(); it != trust.end(); ++it)
		{
			it->second = 0.0;
		}

		// cout << "Calculating trust" << endl;

		// Secondly, calculate trust scores agains
		for (auto user = ratings.begin(); user != ratings.end(); ++user)
		{
			for (auto item = user->second.begin(); item != user->second.end(); ++item)
			{
				trust[user->first] += credibility.at(item->first).at(item->second.rating);
			}
		}

		// cout << "trust calculated" << endl;
	}

	return make_pair(trust, credibility);
}

double nNorm(const vector<double>& input, int n)
{
	double norm = 0.0;
	for (auto val: input)
	{
		norm += pow(val, n);
	}
	return pow(norm, (double)1/n);
}