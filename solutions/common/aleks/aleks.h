#include "ratings.h"
#include <map>
#include <functional>

#ifndef ALEKS_ALGORITHM
#define ALEKS_ALGORITHM

// http://stackoverflow.com/questions/6684573/floating-point-keys-in-stdmap
// Using Custom Comparator for maps, to help with matching floating point keys
// This is generally a bad idea, however as floats are only ratings and suitably far
// apart, for this use case it is passable and hopefully won't cause issues
// also used in aleks.cpp

// class own_double_less : public std::binary_function<double,double,bool>
// {
// public:
//   own_double_less( double arg_ = 1e-7 ) : epsilon(arg_) {}
//   bool operator()( const double &left, const double &right  ) const
//   {
//     // you can choose other way to make decision
//     // (The original version is: return left < right;) 
//     return (abs(left - right) > epsilon) && (left < right);
//   }
//   double epsilon;
// };

// user -> credibility
typedef std::map<unsigned int, double> trust_type;
// movie -> rating given -> credibility
typedef std::map<unsigned int, std::map<double, double>> credibility_type;
//

std::pair<trust_type, credibility_type> aleksAlgo(rating_matrix& ratings, double alpha);

#endif