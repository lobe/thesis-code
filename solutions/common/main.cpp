#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include "ratings.h"
#include "recommender.h"

RatingCollection readRatings(char* fileName);

using namespace std;

int main(int argc, char*argv[])
{

	if (argc < 2) {
		cerr << "Must specify data file as second param" << endl;
		exit(1);
	}

	RatingCollection ratingCollection = readRatings(argv[1]);
	RatingCollection predictedRatings = predictRatings(ratingCollection);
	cout << predictedRatings << endl;

	return 0;
}

RatingCollection readRatings(char* fileName)
{
	vector<Rating> ratings;
	ifstream handle(fileName);
	if (!handle.is_open()) {
		cerr << "Failed to open file" << endl;
		exit(1);
	}
	unsigned int user, movie, rating;

	string line;
	while (getline(handle, line)) {
		istringstream iss(line);
		Rating r;
		if (!(iss >> user >> movie >> rating)) {
			cerr << "INVALID LINE FORMAT" << endl;
			exit(2);
		}
		r.user = user - 1; // 0 index that shizzle
		r.movie = movie - 1; // 0 index that shizzle
		r.rating = scaleRating(rating);
		ratings.push_back(r);
	}

	unsigned int numUsers = 0;
	unsigned int numMovies = 0;
	for (auto i: ratings) {
		numUsers = max(numUsers, i.user + 1);
		numMovies = max(numMovies, i.movie + 1);
	}

	RatingCollection result;
	result.ratings = ratings;
	result.numMovies = numMovies;
	result.numUsers = numUsers;

	return result;
}
