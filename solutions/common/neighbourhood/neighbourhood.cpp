#include "neighbourhood.h"
#include "aleks/aleks.h"
#include <cmath>

using namespace std;

struct ScaledRating
{
	Rating rating;
	double scale;
};

double userDifference(mapped_ratings& a, mapped_ratings& b);
inline double sigmoid(double x)
{
	return 1.0 / (1.0 + exp(-x));
}

mapped_ratings averageVector(vector<mapped_ratings*>& input, double averageRating, unsigned int numMovies);

// take the collection of ratings, and return in structure user -> movie -> rating object (2d map)
grouped_ratings ratingsByUser(RatingCollection& rc)
{
	auto res = grouped_ratings();
	for (auto r: rc.ratings)
	{
		auto userEntry = res.find(r.user);
		if (userEntry == res.end()) // Insert if doesn't exist
		{
			userEntry = res.insert(pair<unsigned int, map<unsigned int, Rating>>(r.user, map<unsigned int, Rating>())).first;
		}
		// Insert rating
		userEntry->second[r.movie] = r;
	}
	return res;
}	

grouped_ratings ratingsByItem(RatingCollection& rc)
{
	auto res = grouped_ratings();
	for (auto r: rc.ratings)
	{
		auto itemEntry = res.find(r.movie);
		if (itemEntry == res.end()) // Insert if doesn't exist
		{
			itemEntry = res.insert(pair<unsigned int, map<unsigned int, Rating>>(r.movie, map<unsigned int, Rating>())).first;
		}
		// Insert rating
		itemEntry->second[r.user] = r;
	}
	return res;
}

sim_matrix simMatrix(grouped_ratings& gr, Comparator *comparator)
{
	sim_matrix res = sim_matrix();
	for (auto u1 = gr.begin(); u1 != gr.end(); ++u1)
	{
		// cout << "Similarity for " << u1->first << endl;
		res[u1->first] = sim_map();
		for (auto u2 = gr.begin(); u2 != gr.end(); ++u2)
		{
			res[u1->first].insert(make_pair(comparator->similarity(u1->second, u2->second), u2->first));
		}
	}
	return res;
}

double CosineComparator::similarity(mapped_ratings& a, mapped_ratings& b)
{
	double dotProduct = 0.0;
	double aNorm = 0.0;
	double bNorm = 0.0;
	unsigned int count = 0;
	unsigned int max_ratings = min(a.size(), b.size());

	// Side by side loop
	auto iterA = a.begin();
	auto iterB = b.begin();
	while (iterA != a.end() && iterB != b.end())
	{
		while(iterA != a.end() && iterB != b.end() && iterA->first < iterB->first)
			++iterA;
		while(iterA != a.end() && iterB != b.end() && iterA->first > iterB->first)
			++iterB;
		while(iterA != a.end() && iterB != b.end() && iterA->first == iterB->first)
		{
			dotProduct += iterA->second.rating * iterB->second.rating;
			aNorm += iterA->second.rating * iterA->second.rating;
			bNorm += iterB->second.rating * iterB->second.rating;
			++count;
			++iterA;
			++iterB;
		}
	}

	// If only 0 or 1 movies match, disregard (as no angle between vectors)
	if (count >= 2 && (aNorm == 0.0 || bNorm == 0.0))
	{
		if (aNorm == bNorm)
			return 1;
		return 0;
	}

	if (count < 2)
		return 0;

	double cosineSimilarity = dotProduct / (sqrt(aNorm) * sqrt(bNorm));

	// Adjust cosine, to give higher score for users who match more movies
	// log portion is (0, 1], cosineSimilarity is (0, 1]
	//return log(1 + count / a.size()) * cosineSimilarity;
	cosineSimilarity *= sigmoid(3 * count / max_ratings);

	return cosineSimilarity;
}

double CosineItemComparator::similarity(mapped_ratings& a, mapped_ratings& b)
{
	double dotProduct = 0.0;
	double aNorm = 0.0;
	double bNorm = 0.0;
	unsigned int count = 0;

	// Side by side loop
	auto iterA = a.begin();
	auto iterB = b.begin();
	while (iterA != a.end() && iterB != b.end())
	{
		while(iterA != a.end() && iterB != b.end() && iterA->first < iterB->first)
			++iterA;
		while(iterA != a.end() && iterB != b.end() && iterA->first > iterB->first)
			++iterB;
		while(iterA != a.end() && iterB != b.end() && iterA->first == iterB->first)
		{
			dotProduct += iterA->second.rating * iterB->second.rating;
			aNorm += iterA->second.rating * iterA->second.rating;
			bNorm += iterB->second.rating * iterB->second.rating;
			++count;
			++iterA;
			++iterB;
		}
	}

	// If only 0 or 1 movies match, disregard (as no angle between vectors)
	if (count >= 2 && (aNorm == 0.0 || bNorm == 0.0))
	{
		if (aNorm == bNorm)
			return 1;
		return 0;
	}

	if (count < 2)
		return 0;

	double cosineSimilarity = dotProduct / (sqrt(aNorm) * sqrt(bNorm));

	return cosineSimilarity;
}

// This topKratings is written from the point of view of user to user sim matrix
// It should work identically if an item -> item sim matrix (and rating collection) is fed in instead
// Just need to flip user and movie when doing this, which the userComparison bool is for
// Fills out the ratings of rc (which should already have numUsers and numMovies set)
// If userComp
void topKRatings(bool userComparison, RatingCollection& rc, sim_matrix& similarityMatrix, grouped_ratings& userRatings, double defaultRating, unsigned int k, bool scaled)
{
	auto averageVector = map<unsigned int, vector<double>>();
	for (auto userIt = userRatings.begin(); userIt != userRatings.end(); ++userIt)
	{
		for (auto movieIt = userIt->second.begin(); movieIt != userIt->second.end(); ++movieIt)
		{
			if (userComparison)
			{
				auto insertIt = averageVector.find(userIt->first);
				if (insertIt == averageVector.end())
					insertIt = averageVector.insert(make_pair(userIt->first, vector<double>())).first;	
				insertIt->second.push_back(movieIt->second.rating);
			} else
			{
				auto insertIt = averageVector.find(movieIt->first);
				if (insertIt == averageVector.end())
					insertIt = averageVector.insert(make_pair(movieIt->first, vector<double>())).first;	
				insertIt->second.push_back(movieIt->second.rating);							
			}
		}
	}
	auto averages = map<unsigned int, double>();
	for (auto it = averageVector.begin(); it != averageVector.end(); ++it)
	{
		double sum = 0.0;
		for (int n : it->second)
    		sum += n;
		averages[it->first] = sum / it->second.size();
	}


	for (unsigned int u = 0; u < rc.numUsers; ++u) {
		// cout << "ratings for user " << u << endl;
		for (unsigned int m = 0; m < rc.numMovies; ++m) {
			// Take top 10 users who have rated that movie, and use their average rating,
			// If none exist, set rating as default
			unsigned int user = u;
			unsigned int movie = m;
			// If this algo is used for item -> item comparison, switch user and movie
			if (!userComparison)
			{
				user = m;
				movie = u;
			}

			auto similarUsers = similarityMatrix[user];
			unsigned int similarFoundCount = 0;
			double ratingTally = 0.0;
			double scalingTally = 0.0;
			// Loop until end of list, no more similar users found, or k entries found
			for (auto userIt = similarUsers.begin(); userIt != similarUsers.end() && userIt->first != 0.0 && similarFoundCount < k; ++userIt)
			{
				// Exclude the same user from matching -> does nothing for predictive power
				if (userIt->second == user)
					continue;

				// Have a similar user at userIt->second, now search for a user
				auto movieIt = userRatings[userIt->second].find(movie);
				if (movieIt != userRatings[userIt->second].end())
				{
					if (userComparison)
					{
						if (scaled)
						{
							ratingTally += (movieIt->second.rating - averages[userIt->second]) * userIt->first;
							scalingTally += userIt->first;
						} 
						else 
						{
							ratingTally += (movieIt->second.rating - averages[userIt->second]);
							scalingTally += 1;						
						}
					}
					else
					{
						if (scaled)
						{
							ratingTally += movieIt->second.rating * userIt->first;
							scalingTally += userIt->first;
						} 
						else 
						{
							ratingTally += movieIt->second.rating;
							scalingTally += 1;						
						}						
					}
					++similarFoundCount;
				}
			}

			Rating rating;
			rating.user = u;
			rating.movie = m;
			if (similarFoundCount > 0)
			{
				if (userComparison)
					rating.rating = averages[user] + ratingTally / scalingTally;
				else
					rating.rating = ratingTally / scalingTally;
			}
			else
			{
				rating.rating = defaultRating;
			}
			rc.ratings.push_back(rating);
		}
	}
}

void topKRatings(bool userComparison, RatingCollection& rc, sim_matrix& similarityMatrix, grouped_ratings& userRatings, double defaultRating, unsigned int k)
{
	bool scaled = userComparison;
	topKRatings(userComparison, rc, similarityMatrix, userRatings, defaultRating, k, scaled);
}

// This topKratings is written from the point of view of user to user sim matrix
// It should work identically if an item -> item sim matrix (and rating collection) is fed in instead
// Just need to flip user and movie when doing this, which the userComparison bool is for
// Fills out the ratings of rc (which should already have numUsers and numMovies set)
void topKRatingsAleks(bool userComparison, RatingCollection& rc, sim_matrix& similarityMatrix, grouped_ratings& userRatings, double defaultRating, unsigned int k, double alpha)
{
	rating_matrix elections; // user -> election -> rating
	map<unsigned int, map<unsigned int, vector<ScaledRating>>> scaledElections; // user -> election -> similarity;

	unsigned int electionCount = 0;

	// cout << "Scanning ratings" << endl;

	for (unsigned int u = 0; u < rc.numUsers; ++u) {
		// cout << "ratings for user " << u << endl;
		for (unsigned int m = 0; m < rc.numMovies; ++m) {
			// Take top 10 users who have rated that movie, and use their average rating,
			// If none exist, set rating as default
			unsigned int user = u;
			unsigned int movie = m;
			// If this algo is used for item -> item comparison, switch user and movie
			if (!userComparison)
			{
				user = m;
				movie = u;
			}

			auto similarUsers = similarityMatrix[user];
			unsigned int similarFoundCount = 0;

			auto electionSim = scaledElections.insert(make_pair(user, map<unsigned int, vector<ScaledRating>>())).first;
			auto specificElection = electionSim->second.insert(make_pair(movie, vector<ScaledRating>())).first;

			// Loop until end of list, no more similar users found, or k entries found
			for (auto userIt = similarUsers.begin(); userIt != similarUsers.end() && userIt->first != 0.0 && similarFoundCount < k; ++userIt)
			{
				// Exclude the same user from matching -> does nothing for predictive power
				if (userIt->second == user)
					continue;

				// Have a similar user at userIt->second, now search for a user
				auto movieIt = userRatings[userIt->second].find(movie);
				if (movieIt != userRatings[userIt->second].end())
				{
					auto electionUser = elections.insert(make_pair(userIt->second, map<unsigned int, Rating>())).first;
					
					Rating temp;
					temp.user = user;
					temp.movie = electionCount;
					temp.rating = movieIt->second.rating;

					electionUser->second[electionCount] = temp;

					ScaledRating tempScaledRating;
					tempScaledRating.rating = temp;
					tempScaledRating.rating.user = userIt->second;
					tempScaledRating.rating.movie = movie;
					tempScaledRating.rating.rating = movieIt->second.rating;
					tempScaledRating.scale = userIt->first;

					specificElection->second.push_back(tempScaledRating);

					++similarFoundCount;
				}
			}
			++electionCount;
		}
	}

	// cout << "Ratings Scanned" << endl;

	auto aleks = aleksAlgo(elections, alpha);
	trust_type trust = aleks.first;

	// double averageTrust = 0.0;
	// unsigned int averageTrustCount = 0;
	// double attackedTrust = 0.0;
	// unsigned int attackedTrustCount = 0;

	// for (auto it = trust.begin(); it != trust.end(); ++it)
	// {
	// 	if (it->first > 942)
	// 	{
	// 		attackedTrust += it->second;
	// 		++attackedTrustCount;
	// 	} else
	// 	{
	// 		averageTrust += it->second;
	// 		++averageTrustCount;
	// 	}
	// }

	// cout << "Legit Trust: " << averageTrust / averageTrustCount << endl;
	// cout << "Dodgy Trust: " << attackedTrust / attackedTrustCount << endl;
	// exit(1);

	for (auto userIt = scaledElections.begin(); userIt != scaledElections.end(); ++userIt)
	{
		// cout << "Reassembling for " << userIt->first << endl;
		for (auto movieIt = userIt->second.begin(); movieIt != userIt->second.end(); ++movieIt)
		{
			double ratingTally = 0.0;
			double scalingTally = 0.0;
			for (auto scaledRating = movieIt->second.begin(); scaledRating != movieIt->second.end(); ++scaledRating)
			{
				ratingTally += scaledRating->rating.rating * scaledRating->scale * trust[scaledRating->rating.user];
				scalingTally += scaledRating->scale * trust[scaledRating->rating.user];
				// ratingTally += scaledRating->rating.rating * scaledRating->scale;
				// scalingTally += scaledRating->scale;
			}

			Rating r;
			r.user = userIt->first;
			r.movie = movieIt->first;
			if (ratingTally == 0.0 && scalingTally == 0.0)
				r.rating = defaultRating;
			else
				r.rating = ratingTally / scalingTally;				
			rc.ratings.push_back(r);
		}
	}

	// cout << "Ratings reassembled" << endl;
}

cluster_t testClusters(sim_matrix& similarity, unsigned int k)
{
	//srand((unsigned)time(NULL)); // Seed rand for goodness - although it shouldn't matter too much
	cluster_t results(k, vector<unsigned int>());

	cout << "Starting cluster" << endl;

	map<unsigned int, map<unsigned int, double>> properSim;
	for (auto sim : similarity)
	{
		properSim[sim.first] = map<unsigned int, double>();
		for (auto userSim : sim.second)
		{
			properSim[sim.first][userSim.second] = userSim.first;
		}
	}

	map<unsigned int, unsigned int> currAnchors; // user -> anchor

	if (similarity.size() < k)
	{
		cout << "Not enough users for k groups";
		exit(1);
	}

	cout << "Initialising random users as anchors" << endl;
	// Initially assign random users to results
	for (unsigned int i = 0; i < k; ++i)
	{
		unsigned int anchorUser = 0;
		while (true)
		{
			anchorUser = rand() % (int)(similarity.size() + 1);
			if (currAnchors.find(anchorUser) == currAnchors.end())
			{
				break;
			}
			cout << "finding size" << endl;
		}
		currAnchors.insert(make_pair(anchorUser, i));
	}

	// Iterate
	unsigned int iterCount = 0;
	while (true)
	{
		cout << "ITERATION " << iterCount << endl;
		++iterCount; 

		results = vector<vector<unsigned int>>(k, vector<unsigned int>());
		// Assign users to the closest group
		for (auto userSim : properSim)
		{
			double highestSim = 0.0;
			unsigned int similarAnchor = 0;
			for (auto& anchors : currAnchors)
			{
				auto currSim = userSim.second[anchors.first];
				if (currSim > highestSim)
				{
					highestSim = currSim;
					similarAnchor = anchors.second;
				}
			}

			results.at(similarAnchor).push_back(userSim.first);
		}

		cout << "Moving anchor to best user" << endl;
		// Move anchor to new user
		auto oldAnchors = currAnchors;
		currAnchors = map<unsigned int, unsigned int>();
		for (auto resIt = results.begin(); resIt != results.end(); ++resIt)
		{
			// Brute force for now
			unsigned int bestAnchor = 0;
			double bestAnchorSim = 0.0;
			for (auto user : *resIt)
			{
				double simTally = 0.0;
				for (auto otherIt = resIt->begin(); otherIt != resIt->end(); ++otherIt)
				{
					simTally += properSim[user][*otherIt];
					// cout << "    properSim[" << user << "][" << *otherIt << "] = " << properSim[user][*otherIt] << endl;
				}
				// cout << "User " << user << ": " << simTally << endl;
				if (simTally > bestAnchorSim)
				{
					bestAnchorSim = simTally;
					bestAnchor = user;
				}
			}
			currAnchors[bestAnchor] = distance(results.begin(), resIt);
		}

		cout << "Testing for change in anchors" << endl;
		// Terminate if no anchors moved
		bool changed = false;
		for (auto oldAnchor : oldAnchors)
		{
			if (currAnchors.find(oldAnchor.first) == currAnchors.end())
				changed = true;
		}

		if (!changed)
			break;
	}

	// Output some stats

	// Bad Eggs
	for (auto firstIt = properSim.begin(); firstIt != properSim.end(); ++firstIt)
	{
		if (firstIt->first > 942)
			cout << "Sim of user 943 and " << firstIt->first << " = " << properSim[943][firstIt->first] << endl;
	}

	double simTallyAverage = 0.0;
	unsigned int simTallyCount = 0;
	for (auto simMap : properSim)
	{
		simTallyAverage += simMap.second[0];
		simTallyCount++;
	}
	cout << "Average sim tally count is " << (double)simTallyAverage / (double)simTallyCount << endl;

	for (auto res : results)
	{
		simTallyAverage = 0.0;
		for (auto user : res)
		{
			simTallyAverage += properSim[res.at(0)][user];
		}
		cout << "Group has average sim of " << simTallyAverage / (double)res.size() << endl;
	}

	return results;

}

// Use kNN to form neighbourhoods of users
// map<unsigned int, vector<double>> knnRatings(rating_matrix& ratings, unsigned int k, unsigned int numUsers, unsigned int numMovies, double averageRating)
// {
// 	srand((unsigned)time(NULL)); // Seed rand for goodness - although it shouldn't matter too much
// 	grouped_ratings points; // Representing the clusters
// 	for (unsigned int i = 0; i < k; ++i)
// 	{
// 		mapped_ratings point;
// 		for (unsigned int x = 0; x < numMovies; ++x)
// 		{
// 			Rating r;
// 			r.user = i;
// 			r.movie = x;
// 			r.rating = ((double)rand()/(double)RAND_MAX); // between 0 and 1
// 			point.insert(make_pair(x, r));
// 		}
// 		points.insert(make_pair(i, point));
// 	}

// 	// Now knn that shizzle. Assign each user to the closest point, recompute points, repeat until no user moves to a new point
// 	map<unsigned int, map<unsigned int, double>> groups; // user -> point -> closeness	
// 	unsigned int iterCount = 0;
// 	while (true)
// 	{
// 		cout << "KNN iteration #" << iterCount << endl;
// 		++iterCount;
// 		unsigned int usersChanged = 0;

// 		bool userChanged = false;
// 		// Move each user to the closest point

// 		for (auto userIt = ratings.begin(); userIt != ratings.end(); ++userIt)
// 		{
// 			auto groupIt = groups.find(userIt->first);
// 			if (groupIt == groups.end())
// 			{
// 				// First run, initialise all points
// 				userChanged = true; // Don't terminate on first run
// 				map<unsigned int, double> closeness;
// 				for (auto pointIt = points.begin(); pointIt != points.end(); ++pointIt)
// 				{
// 					closeness.insert(make_pair(pointIt->first, 0.0));
// 				}
// 				groupIt = groups.insert(make_pair(userIt->first, closeness)).first;
// 			}

// 			// Work out the current closest;
// 			unsigned int closest;
// 			double closestVal;
// 			bool closestSet = false;
// 			for (auto& closePair : groupIt->second)
// 			{
// 				if (!closestSet || closestVal > closePair.second)
// 				{
// 					closestSet = true;
// 					closest = closePair.first;
// 					closestVal = closePair.second;
// 				}
// 			}

// 			unsigned int newClosest;
// 			double newClosestVal;
// 			bool newClosestSet = false;
// 			// Now iterate through and set closeness
// 			for (auto& closePair : groupIt->second)
// 			{
// 				closePair.second = userDifference(points.find(closePair.first)->second, userIt->second);
// 				if (!newClosestSet || closePair.second < newClosestVal)
// 				{
// 					newClosestSet = true;
// 					newClosest = closePair.first;
// 					newClosestVal = closePair.second;
// 				}
// 			}

// 			if (closest != newClosest)
// 			{
// 				++usersChanged;
// 				userChanged = true;
// 			}
// 		}

// 		cout << "Users Changed this iteration: " << usersChanged << endl;

// 		// If no user moved, terminate
// 		if (!userChanged)
// 			break;

// 		// change points to be the average of all users designated to that point
// 		map<unsigned int, vector<mapped_ratings*>> assigned; // point -> users attached to that point
// 		// Attach users to the point
// 		for (auto& point : points)
// 		{
// 			assigned.insert(make_pair(point.first, vector<mapped_ratings*>()));
// 		}
// 		// Now attach users to points
// 		for (auto& user : ratings)
// 		{
// 			auto points = groups.find(user.first);
// 			bool firstRun = true;
// 			unsigned int closestPoint;
// 			double closestVal;
// 			for (auto closenessIt = points->second.begin(); closenessIt != points->second.end(); ++closenessIt)
// 			{
// 				if (firstRun || closenessIt->second < closestVal)
// 				{
// 					firstRun = false;
// 					closestPoint = closenessIt->first;
// 					closestVal = closenessIt->second;
// 				}
// 			}
// 			assigned[closestPoint].push_back(&(user.second));
// 		}
// 		// Now assign everything
// 		for (auto& point : points)
// 		{
// 			point.second = averageVector(assigned[point.first], averageRating, numMovies);
// 		}
// 	}

// 	// Now translate groups into a vector of vectors
// 	auto results = map<unsigned int, vector<double>>();

// 	for (auto group : groups)
// 	{
// 		unsigned int user = group.first;
// 		// I should really rethink this, or move it into a function
// 		bool firstRun = true;
// 		unsigned int closestPoint = 0;
// 		double closestVal = 0.0;
// 		for (auto point : group.second)
// 		{
// 			if (firstRun || point.second < closestVal)
// 			{
// 				firstRun = false;
// 				closestPoint = point.first;
// 				closestVal = point.second;
// 			}
// 		}

// 		// Now that we have the user and the closest point, insert into structure
// 		auto resIt = results.find(closestPoint);
// 		if (resIt == results.end())
// 		{
// 			resIt = results.insert(make_pair(closestPoint, vector<double>())).first;
// 		}
// 		resIt->second.push_back(user);
// 	}

// 	return results;
// }

// double userDifference(mapped_ratings& a, mapped_ratings& b)
// {
// 	double difference = 0.0;
// 	unsigned int count = 0;
// 	// Side by side loop
// 	auto iterA = a.begin();
// 	auto iterB = b.begin();
// 	while (iterA != a.end() && iterB != b.end())
// 	{
// 		while(iterA != a.end() && iterB != b.end() && iterA->first < iterB->first)
// 			++iterA;
// 		while(iterA != a.end() && iterB != b.end() && iterA->first > iterB->first)
// 			++iterB;
// 		while(iterA != a.end() && iterB != b.end() && iterA->first == iterB->first)
// 		{
// 			difference += abs(iterA->second.rating - iterB->second.rating);
// 			++count;
// 			++iterA;
// 			++iterB;
// 		}
// 	}

// 	return difference / count;
// }

// mapped_ratings averageVector(vector<mapped_ratings*>& input, double averageRating, unsigned int numMovies)
// {
// 	// Calculate the average Vector, filling with the averageRating for missing entries
// 	mapped_ratings result;

// 	auto valueIterators = vector<pair<mapped_ratings::iterator, mapped_ratings::iterator>>();
// 	//Add all beginning iterators 
// 	for (auto user : input)
// 	{
// 		valueIterators.push_back(make_pair(user->begin(), user->end()));
// 	}

// 	for (unsigned int currMovie = 0; currMovie < numMovies; ++currMovie)
// 	{
// 		double currMovieTally = 0.0;
// 		unsigned int currMovieCount = 0;
// 		for (auto userRatings : valueIterators)
// 		{
// 			// UserRatings.second is the end
// 			if (userRatings.first != userRatings.second && userRatings.second->second.movie == currMovie)
// 			{
// 				currMovieTally += userRatings.second->second.rating;
// 				++currMovieCount;
// 				++userRatings.second;
// 			}
// 		}
// 		Rating r;
// 		r.user = 0;
// 		r.movie = currMovie;
// 		if (currMovieCount > 0)
// 		{
// 			r.rating = currMovieTally / currMovieCount;
// 			result.insert(make_pair(currMovie, r));
// 		}
// 		else
// 		{
// 			r.rating = averageRating;
// 			result.insert(make_pair(currMovie, r));
// 		}
// 	}

// 	return result;
// }