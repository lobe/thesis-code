#include "ratings.h"
#include <map>

#ifndef NEIGHBOURHOOD_HELPERS
#define NEIGHBOURHOOD_HELPERS

typedef std::map<unsigned int, Rating> mapped_ratings;
typedef std::map<unsigned int, mapped_ratings> grouped_ratings;
typedef std::multimap<double, unsigned int, std::greater<double>> sim_map; // similarity -> user, high to low
typedef std::map<unsigned int, sim_map> sim_matrix; // user -> similarity -> user, or item -> similarity -> item
typedef std::vector<std::vector<unsigned int>> cluster_t;

class Comparator
{
	public:
	virtual double similarity(mapped_ratings& a, mapped_ratings& b) = 0;
	virtual ~Comparator() {}
};

class CosineComparator : public Comparator
{
	double similarity(mapped_ratings& a, mapped_ratings& b);
};

class CosineItemComparator : public Comparator
{
	double similarity(mapped_ratings& a, mapped_ratings& b);	
};

grouped_ratings ratingsByUser(RatingCollection& rc);
grouped_ratings ratingsByItem(RatingCollection& rc);
sim_matrix simMatrix(grouped_ratings& gr, Comparator *comparator);

cluster_t testClusters(sim_matrix& similarity, unsigned int k);

// populate k with predicted ratings based on grouped_ratings and the similarityMatrix
void topKRatings(bool userComparison, RatingCollection& rc, sim_matrix& similarityMatrix, grouped_ratings& userRatings, double defaultRating, unsigned int k);

// Same as above, but filter results through Aleks algo to establish trust
void topKRatingsAleks(bool userComparison, RatingCollection& rc, sim_matrix& similarityMatrix, grouped_ratings& userRatings, double defaultRating, unsigned int k, double alpha);

// // Group a rating matrix into neighbourhoods of similar users via knn
// std::map<unsigned int, std::vector<double>> knnRatings(rating_matrix& ratings, unsigned int k, unsigned int numUsers, unsigned int numMovies, double averageRating);

#endif