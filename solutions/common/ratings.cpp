#include "ratings.h"
#include <iostream>

using namespace std;

ostream& operator << (ostream& o, const Rating& r) {
	o << r.user + 1 << " " << r.movie + 1 << " " << unscaleRatingDouble(r.rating);
	return o;
}

ostream& operator << (ostream& o, const RatingCollection& a) {
	for (auto r: a.ratings) {
		o << r << endl;
	}
	return o;
}

rating_matrix collectionToMatrix(RatingCollection& rc)
{
	auto res = rating_matrix();
	for (auto r: rc.ratings)
	{
		auto userEntry = res.find(r.user);
		if (userEntry == res.end())
		{
			userEntry = res.insert(pair<unsigned int, map<unsigned int, Rating>>(r.user, map<unsigned int, Rating>())).first;
		}
		userEntry->second[r.movie] = r;
	}
	return res;
}

RatingCollection matrixToCollection(rating_matrix& rm)
{
	auto rc = RatingCollection();
	for (auto userEntry: rm)
	{
		for (auto movieEntry: userEntry.second)
		{
			rc.ratings.push_back(movieEntry.second);
			rc.numUsers = max(rc.numUsers, movieEntry.second.user + 1);
			rc.numMovies = max(rc.numMovies, movieEntry.second.movie + 1);
		}
	}
	return rc;
}