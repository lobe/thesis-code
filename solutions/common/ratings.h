#include <vector>
#include <iostream>
#include <map>
#include <unordered_map>
#include <algorithm>

#ifndef RATINGS_H
#define RATINGS_H

struct Rating
{
	unsigned int user; // 0 indexed
	unsigned int movie; // 0 indexed
	double rating;
};

struct RatingCollection
{
	std::vector<Rating> ratings;
	unsigned int numMovies;
	unsigned int numUsers;
};

// Really don't need the value to be Rating, double would suffice, this just makes things simpler
typedef std::map<unsigned int, std::map<unsigned int, Rating>> rating_matrix;

std::ostream& operator<< (std::ostream& o, const RatingCollection& a);
rating_matrix collectionToMatrix(RatingCollection& rc);
RatingCollection matrixToCollection(rating_matrix& rm);

inline double scaleRating(unsigned int rating)
{
	return (double)rating/ 4 - 0.25;
}

inline unsigned int unscaleRating(double rating)
{
	return 4*(rating + 0.25);
}

inline double unscaleRatingDouble(double rating)
{
	return std::min(std::max(4*(rating + 0.25), 1.0), 5.0);
}

#endif