#include "ratings.h"
#include "neighbourhood/neighbourhood.h"
#include <vector>
#include <map>

using namespace std;

RatingCollection predictRatings(RatingCollection rc)
{
	CosineComparator cp = CosineComparator();
	CosineItemComparator cip = CosineItemComparator();

	auto userRatings = ratingsByUser(rc);
	auto userSimMatrix = simMatrix(userRatings, &cp);

	auto itemRatings = ratingsByItem(rc);
	auto itemSimMatrix = simMatrix(itemRatings, &cip);

	// Now take top 10 ratings, average them, and output
	unsigned int userK = 20;
	unsigned int itemK = 10;
	double defaultRating = 0.54;

	// Complete once for ratings based on user -> user

	RatingCollection userRes;

	userRes.numUsers = rc.numUsers;
	userRes.numMovies = rc.numMovies;
	userRes.ratings = vector<Rating>();
	topKRatings(true, userRes, userSimMatrix, userRatings, defaultRating, userK);

	// Now Have another round for rating based on item -> item

	RatingCollection itemRes;

	itemRes.numUsers = rc.numUsers;
	itemRes.numMovies = rc.numMovies;
	itemRes.ratings = vector<Rating>();
	topKRatings(false, itemRes, itemSimMatrix, itemRatings, defaultRating, itemK);

	// Now combine and take average
	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();
	// Invariant: userRes and itemRes have identical entries (except rating values) in same order)
	auto userIt = userRes.ratings.begin();
	auto itemIt = itemRes.ratings.begin();
	while (userIt != userRes.ratings.end())
	{
		Rating r;
		r.user = userIt->user;
		r.movie = userIt->movie;
		r.rating = (userIt->rating + itemIt->rating) / 2;
		res.ratings.push_back(r);
		++userIt;
		++itemIt;
	}

	return res;
}