#include "ratings.h"
#include "neighbourhood/neighbourhood.h"
#include <vector>
#include <cfenv>
#include <algorithm>

using namespace std;

double averageRating(const vector<Rating>& v);
double average(const vector<double>& a);
double zeroMean(vector<double>& v);
void applyDerivative(vector<double>&u, vector<double>& du, double learningRate);
double sigmoid(double x);
void weightDecay(vector<double>& v, double decayRate);

RatingCollection predictRatings(RatingCollection rc)
{
	// First get the average and s0
	double avRating = averageRating(rc.ratings);
	double s0 = -log(1.0/avRating - 1);

	// Round up the neighbourhoods
	CosineComparator cp = CosineComparator();
	CosineItemComparator cip = CosineItemComparator();

	auto userRatings = ratingsByUser(rc);
	auto userSimMatrix = simMatrix(userRatings, &cp);

	auto itemRatings = ratingsByItem(rc);
	auto itemSimMatrix = simMatrix(itemRatings, &cip);

	unsigned int userK = 20;
	unsigned int itemK = 10;

	// Complete once for ratings based on user -> user

	RatingCollection userRes;

	userRes.numUsers = rc.numUsers;
	userRes.numMovies = rc.numMovies;
	userRes.ratings = vector<Rating>();
	topKRatings(true, userRes, userSimMatrix, userRatings, avRating, userK);
	auto userMatrix = collectionToMatrix(userRes);

	// Now Have another round for rating based on item -> item

	RatingCollection itemRes;

	itemRes.numUsers = rc.numUsers;
	itemRes.numMovies = rc.numMovies;
	itemRes.ratings = vector<Rating>();
	topKRatings(false, itemRes, itemSimMatrix, itemRatings, avRating, itemK);
	auto itemMatrix = collectionToMatrix(itemRes);


	// Now onepass with neighbourhood stuff

	vector<double> u = vector<double>(rc.numUsers);
	vector<double> x = vector<double>(rc.numMovies);
	vector<double> du = vector<double>(rc.numUsers);
	vector<double> dx = vector<double>(rc.numMovies);

	double cost = 0.0;
	double lastCost = 1000.0; // Anything but 0.0;
	double learningRate = 0.05;
	unsigned int iterations = 0;
	unsigned int maxIterations = 50000;
	double epsilon = 0.00001;
	double decayRate = 0.9995;

	while(abs(cost - lastCost) > epsilon && iterations < maxIterations)
	{
		fill(du.begin(), du.end(), 0);
		fill(dx.begin(), dx.end(), 0);
		lastCost = cost;
		cost = 0.0; // Only used for debugging / progress checks

		// Loop through all the ratings
		for (auto r : rc.ratings)
		{
			double s = s0 + u.at(r.user) * userMatrix.at(r.user).at(r.movie).rating + x.at(r.movie) * itemMatrix.at(r.user).at(r.movie).rating;
			double z = sigmoid(s);
			double ds = z *( 1 - z ) * ( r.rating - z );
			du.at(r.user) += ds;
			dx.at(r.movie) += ds;

			cost = cost + (r.rating - z) * ( r.rating - z);
		}

		// Now update u and x with the derivatives
		applyDerivative(u, du, learningRate);
		applyDerivative(x, dx, learningRate);
		s0 += zeroMean(u);
		s0 += zeroMean(x);
		weightDecay(u, decayRate);
		weightDecay(x, decayRate);

		++iterations;
		// cout << iterations << "  " << cost << " " << lastCost << " " << abs(cost - lastCost) << endl;
	}

	// Now construct a Ratings Collection from these facts
	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	for (unsigned int user = 0; user < rc.numUsers; ++user) {
		for (unsigned int movie = 0; movie < rc.numMovies; ++movie) {
			Rating rating;
			rating.user = user;
			rating.movie = movie;
			rating.rating = sigmoid(s0 + u.at(user) * userMatrix.at(user).at(movie).rating + x.at(movie) * itemMatrix.at(user).at(movie).rating);
			res.ratings.push_back(rating);
		}
	}

	return res;
}

double averageRating(const vector<Rating>& v)
{
	double sum = accumulate(v.begin(), v.end(), 0.0,
		[&](double sum, const Rating& curr) { return sum + curr.rating; });
	return sum / v.size();
}

double average(const vector<double>& a)
{
	double sum = accumulate(a.begin(), a.end(), 0.0,
		[&](double sum, const double& curr) { return sum + curr; });
	return sum / a.size();
}

// This function relies on the fact that every user/movie is represented at least once (i.e. indexed contiguously)
double zeroMean(vector<double>& v)
{
	double av = average(v);
	transform(v.begin(), v.end(), v.begin(), [&](double& r){ return r -= av;});
	return av;
}

void applyDerivative(vector<double>&u, vector<double>& du, double learningRate)
{
	// transform(u.begin(), u.end(), du.begin(), u.begin(), [](double& a, double& b){ a += b;});
	transform(u.begin(), u.end(), du.begin(), u.begin(), [&](double& a, double& b){return a += learningRate * b;});
}

double sigmoid(double x)
{
	return 1.0 / (1.0 + exp(-x));
}

void weightDecay(vector<double>& v, double decayRate)
{
	transform(v.begin(), v.end(), v.begin(), [&](double& w){ return w *= decayRate; });
}