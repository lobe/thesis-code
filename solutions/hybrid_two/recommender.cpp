#include "ratings.h"
#include "neighbourhood/neighbourhood.h"
#include <vector>
#include <cfenv>
#include "float.h"
#include <algorithm>

using namespace std;

double averageRating(const vector<Rating>& v);
double average(const vector<double>& a);
double zeroMean(vector<double>& v);
void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda, double minValue);
void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda);
double sigmoid(double x);

RatingCollection predictRatings(RatingCollection rc)
{
	// First get the average and s0
	double avRating = averageRating(rc.ratings);
	double s0 = -log(1.0/avRating - 1);

	// Round up the neighbourhoods
	CosineComparator cp = CosineComparator();
	CosineItemComparator cip = CosineItemComparator();

	auto userRatings = ratingsByUser(rc);
	auto userSimMatrix = simMatrix(userRatings, &cp);

	// cout << "Made user sim matrix" << endl;

	auto itemRatings = ratingsByItem(rc);
	auto itemSimMatrix = simMatrix(itemRatings, &cip);

	// cout << "Made item sim matrix" << endl;

	unsigned int userK = 20;
	unsigned int itemK = 10;

	// Complete once for ratings based on user -> user

	RatingCollection userRes;

	userRes.numUsers = rc.numUsers;
	userRes.numMovies = rc.numMovies;
	userRes.ratings = vector<Rating>();
	// for (unsigned int user = 0; user < rc.numUsers; ++user)
	// {
	// 	for (unsigned int movie = 0; movie < rc.numMovies; ++movie)
	// 	{
	// 		Rating r;
	// 		r.user = user;
	// 		r.movie = movie;
	// 		r.rating = ((double) rand() / (RAND_MAX));
	// 		userRes.ratings.push_back(r);
	// 	}
	// }
	topKRatings(true, userRes, userSimMatrix, userRatings, avRating, userK);
	

	auto userMatrix = collectionToMatrix(userRes);

	// cout << "Got topk for user" << endl;

	// Now Have another round for rating based on item -> item

	RatingCollection itemRes;

	itemRes.numUsers = rc.numUsers;
	itemRes.numMovies = rc.numMovies;
	itemRes.ratings = vector<Rating>();
	topKRatings(false, itemRes, itemSimMatrix, itemRatings, avRating, itemK);
	// for (unsigned int user = 0; user < rc.numUsers; ++user)
	// {
	// 	for (unsigned int movie = 0; movie < rc.numMovies; ++movie)
	// 	{
	// 		Rating r;
	// 		r.user = user;
	// 		r.movie = movie;
	// 		r.rating = ((double) rand() / (RAND_MAX));
	// 		itemRes.ratings.push_back(r);
	// 	}
	// }

	auto itemMatrix = collectionToMatrix(itemRes);

	// cout << "got topk for item" << endl;

	// Now onepass with neighbourhood stuff

	vector<double> u = vector<double>(rc.numUsers);
	vector<double> v = vector<double>(rc.numUsers);
	vector<double> x = vector<double>(rc.numMovies);
	vector<double> du = vector<double>(rc.numUsers);
	vector<double> dv = vector<double>(rc.numUsers);
	vector<double> dx = vector<double>(rc.numMovies);

	vector<double> uNeighbour = vector<double>(rc.numUsers);
	vector<double> xNeighbour = vector<double>(rc.numMovies);
	vector<double> duNeighbour = vector<double>(rc.numUsers);
	vector<double> dxNeighbour = vector<double>(rc.numMovies);	

	double cost = 0.0;
	double lastCost = 1000.0; // Anything but 0.0;
	double learningRate = 0.001;
	unsigned int iterations = 0;
	unsigned int maxIterations = 50000;
	double epsilon = 0.00001;

	while(abs(cost - lastCost) > epsilon && iterations < maxIterations)
	{
		double ds0 = 0.0;
		fill(du.begin(), du.end(), 0);
		fill(dv.begin(), dv.end(), 0);
		fill(dx.begin(), dx.end(), 0);
		fill(duNeighbour.begin(), duNeighbour.end(), 0);
		fill(dxNeighbour.begin(), dxNeighbour.end(), 0);
		lastCost = cost;
		cost = 0.0; // Only used for debugging / progress checks

		// Loop through all the ratings
		for (auto r : rc.ratings)
		{
			double s = s0 + u.at(r.user) + x.at(r.movie) + uNeighbour.at(r.user) * userMatrix.at(r.user).at(r.movie).rating + xNeighbour.at(r.movie) * itemMatrix.at(r.user).at(r.movie).rating;
			double z = sigmoid(s);
			double ds = z *( 1 - z ) * ( r.rating - z );

			// double s = s0 + v.at(r.user) + (1 + u.at(r.user)) * x.at(r.movie);
			// double z = sigmoid(s);
			// double ds = r.rating - z;

			// ds *= bin_prob(z, unscaleRating(r.rating));
			// prob += bin_prob(z, unscaleRating(r.rating));

			// dv.at(r.user) += ds;
			ds0 += ds;
			du.at(r.user) += ds;
			dx.at(r.movie) += ds;
			duNeighbour.at(r.user) += ds * userMatrix.at(r.user).at(r.movie).rating;
			dxNeighbour.at(r.movie) += ds * itemMatrix.at(r.user).at(r.movie).rating;

			// du.at(r.user) += ds * x.at(r.movie);
			// dx.at(r.movie) += ds * (1.0 + u.at(r.user));

			cost += (r.rating - z) * ( r.rating - z);
		}

		s0 += 0.01 * learningRate * ds0;

		double lambda0 = 0.3;
		// double lambda1 = 1.0;

		// Now update u and x with the derivatives
		applyDerivative(u, du, learningRate, lambda0);
		// applyDerivative(v, dv, learningRate, lambda0);
		applyDerivative(x, dx, learningRate, lambda0);
		applyDerivative(uNeighbour, duNeighbour, learningRate, lambda0);
		applyDerivative(xNeighbour, dxNeighbour, learningRate, lambda0);

		++iterations;
		// if (iterations % 10 == 0)
		// {
		// 	cout << iterations << endl;
		// 	cout << "    " << cost << " " << lastCost << " " << abs(cost - lastCost) << endl;
		// }
	}

	// Now construct a Ratings Collection from these facts
	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	for (unsigned int user = 0; user < rc.numUsers; ++user) {
		for (unsigned int movie = 0; movie < rc.numMovies; ++movie) {
			Rating r;
			r.user = user;
			r.movie = movie;
			r.rating = s0 + u.at(r.user) + x.at(r.movie) + uNeighbour.at(r.user) * userMatrix.at(r.user).at(r.movie).rating + xNeighbour.at(r.movie) * itemMatrix.at(r.user).at(r.movie).rating;
			r.rating = sigmoid(r.rating);
			res.ratings.push_back(r);
		}
	}

	return res;
}

double averageRating(const vector<Rating>& v)
{
	double sum = accumulate(v.begin(), v.end(), 0.0,
		[&](double sum, const Rating& curr) { return sum + curr.rating; });
	return sum / v.size();
}

double average(const vector<double>& a)
{
	double sum = accumulate(a.begin(), a.end(), 0.0,
		[&](double sum, const double& curr) { return sum + curr; });
	return sum / a.size();
}

// This function relies on the fact that every user/movie is represented at least once (i.e. indexed contiguously)
double zeroMean(vector<double>& v)
{
	double av = average(v);
	transform(v.begin(), v.end(), v.begin(), [&](double& r){ return r -= av;});
	return av;
}

void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda)
{
	applyDerivative(u, du, learningRate, lambda, -DBL_MAX);
}

void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda, double minValue)
{
	transform(u.begin(), u.end(), du.begin(), u.begin(), [&](double& a, double& b){ return max(minValue, a + learningRate * (b - lambda * a));});
}

double sigmoid(double x)
{
	return 1.0 / (1.0 + exp(-x));
}
