#include "ratings.h"
#include "neighbourhood/neighbourhood.h"
#include <vector>
#include <map>

using namespace std;

RatingCollection predictRatings(RatingCollection rc)
{
	CosineItemComparator cip = CosineItemComparator();

	auto itemRatings = ratingsByItem(rc);
	auto itemSimMatrix = simMatrix(itemRatings, &cip);

	unsigned int itemK = 10;
	double defaultRating = 0.54;

	// Have a neighbourhood guess based on item -> item similarity

	RatingCollection itemRes;

	itemRes.numUsers = rc.numUsers;
	itemRes.numMovies = rc.numMovies;
	itemRes.ratings = vector<Rating>();
	topKRatings(false, itemRes, itemSimMatrix, itemRatings, defaultRating, itemK);

	return itemRes;
}