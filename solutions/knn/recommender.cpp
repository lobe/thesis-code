#include "ratings.h"
#include "aleks/aleks.h"
#include "neighbourhood/neighbourhood.h"
#include <vector>
#include <map>

using namespace std;

RatingCollection predictRatings(RatingCollection rc)
{
	rating_matrix inRatings = collectionToMatrix(rc);

	CosineComparator cp = CosineComparator();
	auto userRatings = ratingsByUser(rc);
	auto userSimMatrix = simMatrix(userRatings, &cp);

	unsigned int k = 10;
	cluster_t userClusters = testClusters(userSimMatrix, k);

	for (auto cluster : userClusters)
	{
		unsigned int badEggs = 0;

		for (auto user : cluster)
		{
			if (user >= 943)
			{
				++badEggs;
			}
		}

		cout << "Cluster size is " << cluster.size() << " and has " << badEggs << " bad eggs" << endl;
	}

	auto clusterByUser = map<unsigned int, unsigned int>();
	for (auto clusterIt = userClusters.begin(); clusterIt != userClusters.end(); ++clusterIt)
	{
		unsigned int clusterNum = distance(userClusters.begin(), clusterIt);
		for (auto user : *clusterIt)
		{
			clusterByUser[user] = clusterNum;
		}
	}

	auto clusteredRatings = vector<rating_matrix>(k, rating_matrix());
	for (auto& userRatings : inRatings)
	{
		unsigned int cluster = clusterByUser[userRatings.first];
		clusteredRatings.at(cluster)[userRatings.first] = inRatings[userRatings.first];
	}

	for (auto& cluster : clusteredRatings)
	{
		auto cred = aleksAlgo(cluster, 2.0);
		auto trust = cred.first;
		auto credibility = cred.second;

		unsigned int trustCount = 0;
		unsigned int badCount = 0;
		double trustTally = 0.0;
		double badTrustTally = 0.0;
		for (auto userTrust : trust)
		{
			++trustCount;
			trustTally += userTrust.second;
			if (userTrust.first > 942)
			{
				++badCount;
				badTrustTally += userTrust.second;
			}
		}
		cout << "CLUSTER: " << endl;
		cout << "    Users: " << trustCount << endl;
		cout << "    Bad Users: " << badCount << endl;
		cout << "    Average Trust: " << trustTally / trustCount << endl;
		cout << "    Bad Trust Average: " << badTrustTally / badCount << endl;
	}

	exit(1);
	// auto cred = aleksAlgo(inRatings, 2.0);

	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	double defaultRating = 0.4; // Uniform rating for everyone
	for (unsigned int u = 0; u < rc.numUsers; ++u) {
		for (unsigned int m = 0; m < rc.numMovies; ++m) {
			Rating rating;
			rating.user = u;
			rating.movie = m;
			rating.rating = defaultRating;
			res.ratings.push_back(rating);
		}
	}

	return res;
}