#!/bin/bash

cd "$(dirname "$0")"

while read S || [[ -n "$S" ]]; do
    echo $S
    rm -rf $S/bin # Clean in case bin already exists for some weird reason
    cd $S
    mkdir bin
    cd bin
    cmake ..
    make
    cd ../..
done < solutions.txt
