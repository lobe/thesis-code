#include "ratings.h"
#include "neighbourhood/neighbourhood.h"
#include <vector>
#include <map>

using namespace std;

RatingCollection predictRatings(RatingCollection rc)
{
	// cout << "rearranging by user" << endl;
	auto userRatings = ratingsByUser(rc);
	CosineComparator cp = CosineComparator();
	// cout << "similarity Matrix" << endl;
	auto similarityMatrix = simMatrix(userRatings, &cp);
	// cout << "done with similarity matrix" << endl;

	// Now take top 10 ratings, average them, and output
	unsigned int userK = 20;
	double defaultRating = 0.54;
	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();
	topKRatings(true, res, similarityMatrix, userRatings, defaultRating, userK);

	return res;
}