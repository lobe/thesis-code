#include "ratings.h"
#include "neighbourhood/neighbourhood.h"
#include <vector>
#include <map>

using namespace std;

RatingCollection predictRatings(RatingCollection rc)
{
	auto userRatings = ratingsByUser(rc);
	CosineComparator cp = CosineComparator();
	auto similarityMatrix = simMatrix(userRatings, &cp);

	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	double defaultRating = 0.54; // Uniform rating for everyone, Average??
	for (unsigned int u = 0; u < rc.numUsers; ++u) {
		//cout << "ratings for user " << u << endl;
		for (unsigned int m = 0; m < rc.numMovies; ++m) {
			// Loop through all similar ratings
			// Tally rating (weighted by similarity) and tally similarity weights
			// When done, divide rating by total similarity weight (to get rating back in [0,1])
			// This should weight appropriateley

			auto similarUsers = similarityMatrix[u];
			unsigned int similarFoundCount = 0;
			double ratingTally = 0.0;
			double similarityTally = 0.0;
			// Loop until end of list, no more similar users found, or k entries found
			for (auto userIt = similarUsers.begin(); userIt != similarUsers.end() && userIt->first != 0.0; ++userIt)
			{
				// Have a similar user at userIt->second, now search for a user
				auto movieIt = userRatings[userIt->second].find(m);
				if (movieIt != userRatings[userIt->second].end())
				{
					ratingTally += movieIt->second.rating * userIt->first;
					similarityTally += userIt->first;
					++similarFoundCount;
				}
			}

			Rating rating;
			rating.user = u;
			rating.movie = m;
			if (similarFoundCount > 0)
				rating.rating = ratingTally / similarityTally;
			else
				rating.rating = defaultRating;
			res.ratings.push_back(rating);
		}
	}

	return res;
}