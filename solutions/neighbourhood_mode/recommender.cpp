#include "ratings.h"
#include "neighbourhood/neighbourhood.h"
#include <vector>
#include <map>

using namespace std;

RatingCollection predictRatings(RatingCollection rc)
{
	// cout << "rearranging by user" << endl;
	auto userRatings = ratingsByUser(rc);
	CosineComparator cp = CosineComparator();
	// cout << "similarity Matrix" << endl;
	auto similarityMatrix = simMatrix(userRatings, &cp);
	// cout << "done with similarity matrix" << endl;

	// Now take top 10 ratings, average them, and output
	unsigned int k = 10;
	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	double defaultRating = 0.54; // Uniform rating for everyone, Average??
	for (unsigned int u = 0; u < rc.numUsers; ++u) {
		// cout << "ratings for user " << u << endl;
		for (unsigned int m = 0; m < rc.numMovies; ++m) {
			// Take top 10 users who have rated that movie, and use their average rating,
			// If none exist, set rating as 0.5

			auto similarUsers = similarityMatrix[u];
			unsigned int similarFoundCount = 0;
			map<double, unsigned int> ratingMap = {{0.0, 0}, {0.25, 0}, {0.5, 0}, {0.75, 0}, {1.0, 0}};
			// Loop until end of list, no more similar users found, or k entries found
			for (auto userIt = similarUsers.begin(); userIt != similarUsers.end() && userIt->first != 0.0 && similarFoundCount < k; ++userIt)
			{
				// Have a similar user at userIt->second, now search for a user
				auto movieIt = userRatings[userIt->second].find(m);
				if (movieIt != userRatings[userIt->second].end())
				{
					ratingMap[movieIt->second.rating]++;
					++similarFoundCount;
				}
			}

			Rating rating;
			rating.user = u;
			rating.movie = m;
			rating.rating = 0.0;
			if (similarFoundCount > 0)
			{
				unsigned int maxFound = 0;
				auto ratingVec = vector<double>();
				for (auto it = ratingMap.begin(); it != ratingMap.end(); ++it)
				{
					if (it->second > maxFound)
					{
						ratingVec.clear();
						maxFound = it->second;
						ratingVec.push_back(it->first);
					} 
					else if (it->second == maxFound)
					{
						ratingVec.push_back(it->first);
					}
				}
				for (auto it = ratingVec.begin(); it != ratingVec.end(); ++it)
				{
					rating.rating += *it;
				}
				rating.rating /= ratingVec.size();
			}
			else
				rating.rating = defaultRating;
			res.ratings.push_back(rating);
		}
	}

	return res;
}