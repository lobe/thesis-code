CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
PROJECT(onepass)

LIST(APPEND CMAKE_CXX_FLAGS "--std=c++11 -O3 -ffast-math -Wall")
#LIST(APPEND CMAKE_CXX_FLAGS "-g --std=c++11 -ffast-math -Wall")

INCLUDE_DIRECTORIES(../common/)
FILE(GLOB SOURCES "*.cpp" "../common/*.cpp" "../common/*.h")
ADD_EXECUTABLE(${CMAKE_PROJECT_NAME} ${SOURCES})
