#include "ratings.h"
#include "float.h"
#include <vector>
#include <cfenv>
#include <algorithm>

#define  MAX_INT_PLUS_ONE  2147483648.0

using namespace std;

double averageRating(const vector<Rating>& v);
double average(const vector<double>& a);
double zeroMean(vector<double>& v);
void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda, double minValue);
void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda);
double sigmoid(double x);
void weightDecay(vector<double>& v, double decayRate);
double bin_prob(double z, unsigned int rating);

RatingCollection predictRatings(RatingCollection rc)
{
	double avRating = averageRating(rc.ratings);
	double s0 = -log(1.0/avRating - 1);

	vector<double> u = vector<double>(rc.numUsers);
	vector<double> x = vector<double>(rc.numMovies);
	vector<double> v = vector<double>(rc.numUsers);
	vector<double> du = vector<double>(rc.numUsers);
	vector<double> dx = vector<double>(rc.numMovies);
	vector<double>dv = vector<double>(rc.numUsers);

	double cost = 0.0;
	double lastCost = 1000.0; // Anything but 0.0;
	double prob = 0.0;
	double lastProb = 1000.0;	
	double learningRate = 0.001;
	unsigned int iterations = 0;
	unsigned int maxIterations = 50000;
	double epsilon = 0.00000001;
	// double decayRate = 0.9995;

	while((abs(cost - lastCost) > epsilon || abs(prob - lastProb) > epsilon) && iterations < maxIterations)
	{
		double ds0 = 0.0;
		fill(du.begin(), du.end(), 0);
		fill(dx.begin(), dx.end(), 0);
		fill(dv.begin(), dv.end(), 0);
		lastCost = cost;
		lastProb = prob;
		prob = 0.0;
		cost = 0.0; // Only used for debugging / progress checks

		// // Loop through all the ratings
		// double *lastds = (double*)malloc(rc.ratings.size() * sizeof(double));
		// int count = 0;
		for (auto r : rc.ratings)
		{
			double s = s0 + v.at(r.user) + (1 + u.at(r.user)) * x.at(r.movie);
			double z = sigmoid(s);
			double ds = r.rating - z;
			// double ds = z *( 1 - z ) * ( r.rating - z );

			ds *= bin_prob(z, unscaleRating(r.rating));
			prob += bin_prob(z, unscaleRating(r.rating));

			cost += (r.rating - z) * ( r.rating - z);

			// lastds[count] = ds;
			// ++count;

			ds0 += ds;
			dv.at(r.user) += ds;
			du.at(r.user) += ds * x.at(r.movie);
			dx.at(r.movie) += ds * (1.0 + u.at(r.user));
		}

		// for (auto i = 0; i < rc.ratings.size(); ++i)
		// 	cout << i << " " << lastds[i] << endl;
		// exit(1);

		// Now update u and x with the derivatives
		// and weight decay


		s0 += 0.01 * learningRate * ds0;

		double lambda0 = 0.1;
		double lamdba1 = 1.0;

		applyDerivative(v, dv, learningRate, lambda0);
		applyDerivative(u, du, learningRate, lamdba1, -1.0);
		applyDerivative(x, dx, learningRate, lambda0);

		++iterations;
		//if (iterations % 1000 == 0)
		//{
		//	cout << iterations << endl;
		//	cout << "    " << cost << " " << lastCost << " " << abs(cost - lastCost) << endl;
		//	cout << "    " << prob << " " << lastProb << " " << abs(prob - lastProb) << endl;
		//}	
	}

	// Now construct a Ratings Collection from these facts
	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	for (unsigned int user = 0; user < rc.numUsers; ++user) {
		for (unsigned int movie = 0; movie < rc.numMovies; ++movie) {
			Rating rating;
			rating.user = user;
			rating.movie = movie;
			rating.rating = sigmoid(s0 + v.at(user) + (1 + u.at(user)) * x.at(movie));
			res.ratings.push_back(rating);
		}
	}

	return res;
}

double averageRating(const vector<Rating>& v)
{
	double sum = accumulate(v.begin(), v.end(), 0.0,
		[&](double sum, const Rating& curr) { return sum + curr.rating; });
	return sum / v.size();
}

double average(const vector<double>& a)
{
	double sum = accumulate(a.begin(), a.end(), 0.0,
		[&](double sum, const double& curr) { return sum + curr; });
	return sum / a.size();
}

// This function relies on the fact that every user/movie is represented at least once (i.e. indexed contiguously)
double zeroMean(vector<double>& v)
{
	double av = average(v);
	transform(v.begin(), v.end(), v.begin(), [&](double& r){ return r -= av;});
	return av;
}

void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda)
{
	applyDerivative(u, du, learningRate, lambda, -DBL_MAX);
}

void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda, double minValue)
{
	transform(u.begin(), u.end(), du.begin(), u.begin(), [&](double& a, double& b){ return max(minValue, a + learningRate * (b - lambda * a));});
	// for (unsigned int i = 0; i < u.size(); ++i)
	// {
	// 	du[i] -= lambda * u[i];
	// 	u[i] += learningRate * du[i];
	// 	// u[i] = max(minValue, u[i]);
	// }
}

double sigmoid(double x)
{
	return 1.0 / (1.0 + exp(-x));
}

void weightDecay(vector<double>& v, double decayRate)
{
	transform(v.begin(), v.end(), v.begin(), [&](double& w){ return w *= decayRate; });
}

double bin_prob(double z, unsigned int rating)
{
	double prob = 0.0;
	switch( rating ) {
		case 1: prob =   (1-z)*(1-z)*(1-z)*(1-z); break;
		case 2: prob = 4*(1-z)*(1-z)*(1-z)* z; break;
		case 3: prob = 6*(1-z)*(1-z)*  z  * z; break;
		case 4: prob = 4*(1-z)*  z  *  z  * z; break;
		case 5: prob =     z  *  z  *  z  * z; break;

		default:
			cout << "SHOULD NEVER HAVE CALLED bin_prob with rating = " << rating << endl;
			exit(1);
	}

	return prob;
}
