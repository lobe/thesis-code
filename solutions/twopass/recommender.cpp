#include "ratings.h"
#include <vector>
#include <cfenv>
#include <algorithm>

using namespace std;

double averageRating(const vector<Rating>& v);
double average(const vector<double>& a);
double zeroMean(vector<double>& v);
void applyDerivative(vector<double>&u, vector<double>& du, double learningRate);
double sigmoid(double x);
void weightDecay(vector<double>& v, double decayRate);
void applyNoise(vector<double>& v);
double random_gaussian();

RatingCollection predictRatings(RatingCollection rc)
{
	double avRating = averageRating(rc.ratings);
	double s0 = -log(1.0/avRating - 1);

	vector<double> u = vector<double>(rc.numUsers);
	vector<double> x = vector<double>(rc.numMovies);
	vector<double> du = vector<double>(rc.numUsers);
	vector<double> dx = vector<double>(rc.numMovies);

	double cost = 0.0;
	double lastCost = 1000.0; // Anything but 0.0;
	double learningRate = 0.01;
	unsigned int iterations = 0;
	unsigned int maxIterations = 50000;
	unsigned int minIterations = 5000;
	double epsilon = 0.00000001;
	double decayRate = 0.9995;

	while(iterations < minIterations || (abs(cost - lastCost) > epsilon && iterations < maxIterations))
	{
		fill(du.begin(), du.end(), 0);
		fill(dx.begin(), dx.end(), 0);
		lastCost = cost;
		cost = 0.0; // Only used for debugging / progress checks

		// Loop through all the ratings
		for (auto r : rc.ratings)
		{
			double s = s0 + u.at(r.user) + x.at(r.movie);
			double z = sigmoid(s);
			double ds = z *( 1 - z ) * ( r.rating - z );
			du.at(r.user) += ds;
			dx.at(r.movie) += ds;

			cost = cost + (r.rating - z) * ( r.rating - z);
		}

		// Now update u and x with the derivatives
		applyDerivative(u, du, learningRate);
		applyDerivative(x, dx, learningRate);
		s0 += zeroMean(u);
		s0 += zeroMean(x);
		weightDecay(u, decayRate);
		weightDecay(x, decayRate);

		++iterations;
		//cout << iterations << "  " << cost << " " << lastCost << " " << cost - lastCost << endl;
	}

	// Now stage 2 - use crossproduct parameters (that link movies with users)
	// unsigned int numDimensions = 2;
	cost = 0.0;
	lastCost = 1000.0;
	iterations = 0;
	minIterations = 10000;
	maxIterations = 1000000;

	// How many cross products to calculate??
	unsigned int numCrossProducts = 2;

	// Now need a multidimensional datastructure to hold several u and x. Set bias for sets 0, initialse all others to 0
	auto uBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numUsers));
	auto xBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numMovies));
	auto duBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numUsers)); // Plus one as a need for the base bias
	auto dxBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numMovies)); // Plus one as a need for the base bias
	// Check later -- does this cause a memory leak? as original vector not destroyed??
	uBias.at(0) = u;
	xBias.at(0) = x;

	// Seed u(1..numProducts) and x(1..numProducts) with some random noise to kickstart (but only for cross products)
	for (auto k = uBias.begin() + 1; k != uBias.end(); ++k)
	{
		applyNoise(*k);
	}
	for (auto k = xBias.begin() + 1; k != xBias.end(); ++k)
	{
		applyNoise(*k);
	}


	while(iterations < minIterations || (abs(cost - lastCost) > epsilon && iterations < maxIterations))
	{
		// Clear derivatives
		for (auto& k: duBias)
		{
			fill(k.begin(), k.end(), 0);
		}
		for (auto& k: dxBias)
		{
			fill(k.begin(), k.end(), 0);
		}

		lastCost = cost;
		cost = 0.0;

		for (auto r: rc.ratings)
		{
			double s = s0 + uBias.at(0).at(r.user) + xBias.at(0).at(r.movie);
			for (unsigned int k = 1; k <= numCrossProducts; ++k)
			{
				s += uBias.at(k).at(r.user) * xBias.at(k).at(r.movie);
			}
			double z = sigmoid(s);
			double ds = z *( 1 - z ) * ( r.rating - z );
			duBias.at(0).at(r.user) += ds;
			dxBias.at(0).at(r.movie) += ds;
			for (unsigned int k = 1; k <= numCrossProducts; ++k)
			{
				duBias.at(k).at(r.user) += ds * xBias.at(k).at(r.movie);
				dxBias.at(k).at(r.movie) += ds * uBias.at(k).at(r.user);
			}

			cost = cost + (r.rating - z) * ( r.rating - z);
		}

		for (unsigned int k = 0; k <= numCrossProducts; ++k)
		{
			applyDerivative(uBias.at(k), duBias.at(k), learningRate);
			applyDerivative(xBias.at(k), dxBias.at(k), learningRate);
		}

		s0 += zeroMean(uBias.at(0));
		s0 += zeroMean(xBias.at(0));
		weightDecay(uBias.at(0), decayRate);
		weightDecay(xBias.at(0), decayRate);
		for (unsigned int k = 1; k <= numCrossProducts; ++k)
		{
			zeroMean(uBias.at(k));
			zeroMean(xBias.at(k));
			weightDecay(uBias.at(k), decayRate);
			weightDecay(xBias.at(k), decayRate);
		}

		++iterations;
		// cout << iterations << "  " << s0 << " " << cost << " " << lastCost << " " << cost - lastCost << endl;
	}

	// Now construct a Ratings Collection from these facts
	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	for (unsigned int user = 0; user < rc.numUsers; ++user) {
		for (unsigned int movie = 0; movie < rc.numMovies; ++movie) {
			Rating rating;
			rating.user = user;
			rating.movie = movie;
			double crossProductBias = 0.0;
			for (unsigned int k = 1; k <= numCrossProducts; ++k)
			{
				crossProductBias += uBias.at(k).at(user) * xBias.at(k).at(movie);
			}
			rating.rating = sigmoid(s0 + uBias.at(0).at(user) + xBias.at(0).at(movie) + crossProductBias);
			res.ratings.push_back(rating);
		}
	}

	return res;
}

double averageRating(const vector<Rating>& v)
{
	double sum = accumulate(v.begin(), v.end(), 0.0,
		[&](double sum, const Rating& curr) { return sum + curr.rating; });
	return sum / v.size();
}

double average(const vector<double>& a)
{
	double sum = accumulate(a.begin(), a.end(), 0.0,
		[&](double sum, const double& curr) { return sum + curr; });
	return sum / a.size();
}

// This function relies on the fact that every user/movie is represented at least once (i.e. indexed contiguously)
double zeroMean(vector<double>& v)
{
	double av = average(v);
	transform(v.begin(), v.end(), v.begin(), [&](double& r){ return r - av;});
	return av;
}

void applyDerivative(vector<double>&u, vector<double>& du, double learningRate)
{
	// transform(u.begin(), u.end(), du.begin(), u.begin(), [](double& a, double& b){ a += b;});
	transform(u.begin(), u.end(), du.begin(), u.begin(), [&](double& a, double& b){return a + learningRate * b;});
}

void applyNoise(vector<double>& v)
{
	transform(v.begin(), v.end(), v.begin(), [&](double& w){ return 0.0001 * random_gaussian(); });
}

double sigmoid(double x)
{
	return 1.0 / (1.0 + exp(-x));
}

void weightDecay(vector<double>& v, double decayRate)
{
	transform(v.begin(), v.end(), v.begin(), [&](double& w){ return w * decayRate; });
}

/********************************************************//**
   Return random variable from a Gaussian distribution
*/
double random_gaussian()
{
  static double V1, V2, S;
  static int phase = 0;
  double X;
  if(phase == 0) {
    do {
      double U1 =  random() / 2147483648.0;
      double U2 =  random() / 2147483648.0;
      V1 = 2.0 * U1 - 1.0;
      V2 = 2.0 * U2 - 1.0;
      S = V1 * V1 + V2 * V2;
    } while( S >= 1.0 || S == 0.0 );

    X = V1 * sqrt(-2.0 * log(S) / S);
  }
  else {
    X = V2 * sqrt(-2.0 * log(S) / S);
  }
  phase = 1 - phase;

  return X;
}