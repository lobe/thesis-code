#include "ratings.h"
#include "float.h"
#include <vector>
#include <cfenv>
#include <algorithm>

#define  MAX_INT_PLUS_ONE  2147483648.0

using namespace std;

double averageRating(const vector<Rating>& v);
double average(const vector<double>& a);
double zeroMean(vector<double>& v);
void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda, double minValue);
void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda);
double sigmoid(double x);
void weightDecay(vector<double>& v, double decayRate);
double bin_prob(double z, unsigned int rating);
void applyNoise(vector<double>& v);
double random_gaussian();

RatingCollection predictRatings(RatingCollection rc)
{
	double avRating = averageRating(rc.ratings);
	double s0 = -log(1.0/avRating - 1);

	vector<double> u = vector<double>(rc.numUsers);
	vector<double> x = vector<double>(rc.numMovies);
	vector<double> v = vector<double>(rc.numUsers);
	vector<double> du = vector<double>(rc.numUsers);
	vector<double> dx = vector<double>(rc.numMovies);
	vector<double>dv = vector<double>(rc.numUsers);

	double cost = 0.0;
	double lastCost = 1000.0; // Anything but 0.0;
	double prob = 0.0;
	double lastProb = 1000.0;	
	double learningRate = 0.001;
	unsigned int iterations = 0;
	unsigned int maxIterations = 50000;
	// unsigned int maxIterations = 1000;
	double epsilon = 0.000001;
	double lambda0 = 0.1;
	double lambda1 = 1.0;
	// double decayRate = 0.9995;

	while((abs(cost - lastCost) > epsilon || abs(prob - lastProb) > epsilon) && iterations < maxIterations)
	{
		double ds0 = 0.0;
		fill(du.begin(), du.end(), 0);
		fill(dx.begin(), dx.end(), 0);
		fill(dv.begin(), dv.end(), 0);
		lastCost = cost;
		lastProb = prob;
		prob = 0.0;
		cost = 0.0; // Only used for debugging / progress checks

		// Loop through all the ratings
		for (auto r : rc.ratings)
		{
			double s = s0 + v.at(r.user) + (1 + u.at(r.user)) * x.at(r.movie);
			double z = sigmoid(s);
			double ds = r.rating - z;
			// double ds = z *( 1 - z ) * ( r.rating - z );

			ds *= bin_prob(z, unscaleRating(r.rating));
			prob += bin_prob(z, unscaleRating(r.rating));

			cost += (r.rating - z) * ( r.rating - z);

			ds0 += ds;
			dv.at(r.user) += ds;
			du.at(r.user) += ds * x.at(r.movie);
			dx.at(r.movie) += ds * (1.0 + u.at(r.user));
		}

		// Now update u and x with the derivatives
		// and weight decay

		s0 += 0.01 * learningRate * ds0;

		applyDerivative(v, dv, learningRate, lambda0);
		applyDerivative(u, du, learningRate, lambda1, -1.0);
		applyDerivative(x, dx, learningRate, lambda0);

		++iterations;
		// if (iterations % 100 == 0)
		// {
		// 	cout << iterations << endl;
		// 	cout << "    " << cost << " " << lastCost << " " << abs(cost - lastCost) << endl;
		// 	cout << "    " << prob << " " << lastProb << " " << abs(prob - lastProb) << endl;
		// }
	}

	// Now stage 2 - use crossproduct parameters (that link movies with users)
	// unsigned int numDimensions = 2;
	cost = 0.0;
	lastCost = 1000.0;
 	prob = 0.0;
	lastProb = 1000.0;	
	learningRate = 0.001;
	iterations = 0;
	maxIterations = 1000000;

	// How many cross products to calculate??
	unsigned int numCrossProducts = 6;

	// Now need a multidimensional datastructure to hold several u and x. Set bias for sets 0, initialse all others to 0
	auto uBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numUsers));
	auto xBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numMovies));
	auto duBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numUsers)); // Plus one as a need for the base bias
	auto dxBias = vector<vector<double>>(numCrossProducts + 1, vector<double>(rc.numMovies)); // Plus one as a need for the base bias
	// Check later -- does this cause a memory leak? as original vector not destroyed??
	uBias.at(0) = u;
	xBias.at(0) = x;
	duBias.at(0) = du;
	dxBias.at(0) = du;

	// Seed u(1..numProducts) and x(1..numProducts) with some random noise to kickstart (but only for cross products)
	for (auto k = uBias.begin() + 1; k != uBias.end(); ++k)
	{
		applyNoise(*k);
	}
	for (auto k = xBias.begin() + 1; k != xBias.end(); ++k)
	{
		applyNoise(*k);
	}

	// for (unsigned int i = 1; i < numCrossProducts + 1; ++i)
	// {
	// 	for (auto iter = uBias[i].begin(); iter != uBias[i].end(); ++iter)
	// 		*iter = 0.001 * i;
	// 	for (auto iter = xBias[i].begin(); iter != xBias[i].end(); ++iter)
	// 		*iter = 0.001 * i;		
	// }

	while((abs(cost - lastCost) > epsilon || abs(prob - lastProb) > epsilon) && iterations < maxIterations)
	{
		double ds0 = 0.0;
		for (unsigned int i = 0; i < numCrossProducts + 1; ++i)
		{
			fill(duBias[i].begin(), duBias[i].end(), 0);
			fill(dxBias[i].begin(), dxBias[i].end(), 0);
		}
		fill(dv.begin(), dv.end(), 0);
		fill(du.begin(), du.end(), 0);
		fill(dx.begin(), dx.end(), 0);
		lastCost = cost;
		lastProb = prob;
		prob = 0.0;
		cost = 0.0; // Only used for debugging / progress checks

		// Loop through all the ratings
		for (auto r : rc.ratings)
		{
			double s = s0 + v[r.user] + (1.0 + u[r.user]) * x[r.movie];
			for (unsigned int i = 1; i < numCrossProducts + 1; ++i)
			{
				s += uBias[i][r.user] * xBias[i][r.movie];
			}
			double z = sigmoid(s);
			double ds = r.rating - z;
			// double ds = z *( 1 - z ) * ( r.rating - z );

			ds *= bin_prob(z, unscaleRating(r.rating));
			prob += bin_prob(z, unscaleRating(r.rating));

			cost += (r.rating - z) * ( r.rating - z);

			ds0 += ds;
			dv[r.user] += ds;
			du[r.user] += ds * x[r.movie];
			dx[r.movie] += ds * (1.0 + u[r.user]);
			for (unsigned int i = 1; i < numCrossProducts + 1; ++i)
			{
				duBias[i][r.user] += ds * xBias[i][r.movie];
				dxBias[i][r.movie] += ds * uBias[i][r.user];
			}

			// if (r.user == 0 && r.movie == 0)
			// {
			// 	cout << "old " <<  iterations + 1 << " u[" << 1 << "] " << u[0] << endl; 
			// 	cout << "old " << iterations + 1 << " du[" << 1 << "] " << du[0] << endl; 
			// }
			// cout << iterations + 1 << " " << r.user << " " << r.movie << " " << ds << " " << du[r.user] << endl;
		}

		// if (iterations == 4)
		// 	exit(1);

		s0 += 0.01 * learningRate * ds0;

		applyDerivative(v, dv, learningRate, lambda0);
		applyDerivative(u, du, learningRate, lambda1, -1.0);
		applyDerivative(x, dx, learningRate, lambda0);
		for (unsigned int i = 1; i < numCrossProducts + 1; ++i)
		{
			// uBias and xBias should be same size (numCrossProducts + 1)
			applyDerivative(uBias[i], duBias[i], learningRate, lambda1);
			applyDerivative(xBias[i], dxBias[i], learningRate, lambda1);
		}

		// cout << "new " << iterations + 1 << " u[" << 1 << "] " << u[0] << endl << endl; 
		// for (unsigned int i = 0; i < u.size(); ++i)
		// 	cout << iterations + 1 << " u[" << i + 1 << "] " << u[i] << endl; 
		// for (unsigned int i = 0; i < v.size(); ++i)
		// 	cout << iterations + 1 << " v[" << i + 1 << "] " << v[i] << endl; 
		// for (unsigned int i = 0; i < x.size(); ++i)
		// 	cout << iterations + 1 << " x[" << i + 1 << "] " << x[i] << endl; 

		++iterations;
		// if (iterations % 100 == 0)
		// {
		// 	cout << iterations << endl;
		// 	cout << "    " << cost << " " << lastCost << " " << abs(cost - lastCost) << endl;
		// 	cout << "    " << prob << " " << lastProb << " " << abs(prob - lastProb) << endl;
		// }
	}

	// Now construct a Ratings Collection from these facts
	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	for (unsigned int user = 0; user < rc.numUsers; ++user) {
		for (unsigned int movie = 0; movie < rc.numMovies; ++movie) {
			Rating rating;
			rating.user = user;
			rating.movie = movie;
			double s = s0 + v.at(user) + (1 + u.at(user)) * x.at(movie);
			for (unsigned int i = 1; i < numCrossProducts + 1; ++i)
			{
				s += uBias[i][user] * xBias[i][movie];
			}
			rating.rating = sigmoid(s);
			res.ratings.push_back(rating);
		}
	}

	return res;
}

double averageRating(const vector<Rating>& v)
{
	double sum = accumulate(v.begin(), v.end(), 0.0,
		[&](double sum, const Rating& curr) { return sum + curr.rating; });
	return sum / v.size();
}

double average(const vector<double>& a)
{
	double sum = accumulate(a.begin(), a.end(), 0.0,
		[&](double sum, const double& curr) { return sum + curr; });
	return sum / a.size();
}

// This function relies on the fact that every user/movie is represented at least once (i.e. indexed contiguously)
double zeroMean(vector<double>& v)
{
	double av = average(v);
	transform(v.begin(), v.end(), v.begin(), [&](double& r){ return r -= av;});
	return av;
}

void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda)
{
	applyDerivative(u, du, learningRate, lambda, -DBL_MAX);
}

void applyDerivative(vector<double>&u, vector<double>& du, double learningRate, double lambda, double minValue)
{
	transform(u.begin(), u.end(), du.begin(), u.begin(), [&](double& a, double& b){ return max(minValue, a + learningRate * (b - lambda * a));});
	// for (unsigned int i = 0; i < u.size(); ++i)
	// {
	// 	du[i] -= lambda * u[i];
	// 	u[i] += learningRate * du[i];
	// 	// u[i] = max(minValue, u[i]);
	// }
}

double sigmoid(double x)
{
	return 1.0 / (1.0 + exp(-x));
}

void weightDecay(vector<double>& v, double decayRate)
{
	transform(v.begin(), v.end(), v.begin(), [&](double& w){ return w *= decayRate; });
}

void applyNoise(vector<double>& v)
{
	transform(v.begin(), v.end(), v.begin(), [&](double& w){ return 0.1 * random_gaussian(); });
}

double bin_prob(double z, unsigned int rating)
{
	double prob = 0.0;
	switch( rating ) {
		case 1: prob =   (1-z)*(1-z)*(1-z)*(1-z); break;
		case 2: prob = 4*(1-z)*(1-z)*(1-z)* z; break;
		case 3: prob = 6*(1-z)*(1-z)*  z  * z; break;
		case 4: prob = 4*(1-z)*  z  *  z  * z; break;
		case 5: prob =     z  *  z  *  z  * z; break;

		default:
			cout << "SHOULD NEVER HAVE CALLED bin_prob with rating = " << rating << endl;
			exit(1);
	}

	return prob;
}

/********************************************************//**
   Return random variable from a Gaussian distribution
*/
double random_gaussian()
{
  static double V1, V2, S;
  static int phase = 0;
  double X;
  if(phase == 0) {
    do {
      double U1 =  random() / 2147483648.0;
      double U2 =  random() / 2147483648.0;
      V1 = 2.0 * U1 - 1.0;
      V2 = 2.0 * U2 - 1.0;
      S = V1 * V1 + V2 * V2;
    } while( S >= 1.0 || S == 0.0 );

    X = V1 * sqrt(-2.0 * log(S) / S);
  }
  else {
    X = V2 * sqrt(-2.0 * log(S) / S);
  }
  phase = 1 - phase;

  return X;
}
