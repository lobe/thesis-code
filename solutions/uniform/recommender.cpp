#include "ratings.h"
#include <vector>

using namespace std;

RatingCollection predictRatings(RatingCollection rc)
{
	RatingCollection res;

	res.numUsers = rc.numUsers;
	res.numMovies = rc.numMovies;
	res.ratings = vector<Rating>();

	double defaultRating = 0.4; // Uniform rating for everyone
	for (unsigned int u = 0; u < rc.numUsers; ++u) {
		for (unsigned int m = 0; m < rc.numMovies; ++m) {
			Rating rating;
			rating.user = u;
			rating.movie = m;
			rating.rating = defaultRating;
			res.ratings.push_back(rating);
		}
	}

	return res;
}