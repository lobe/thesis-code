#!/bin/bash

cd `dirname $0`

solutions/makeall.sh
pyscripts/remote/executor.sh attacks solutions/solutions.txt pyscripts/remote/analysis.py pyscripts/remote/aggregateAnalysis.py . 2>&1 | tee executor.log
